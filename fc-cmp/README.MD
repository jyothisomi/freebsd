
------------------------------------------------------------------------------------------------------------------

Instructions to setup sqlcipher encrypted database using sqlite
The following commands can be run from home directory on a Linux box. 

step1: Install dependencies: sudo apt-get install libssl-dev
                             sudo apt-get install tcl
step2: git clone https://github.com/sqlcipher/sqlcipher
step3: cd sqlcipher
step4: ./configure --enable-tempstore=yes CFLAGS="-DSQLITE_HAS_CODEC" LDFLAGS="-lcrypto"
step5: make
step6: sudo make install
step7: check installed properly by typing : sqlcipher
step8: pip3 install pysqlcipher3
step9: sudo cp /usr/local/lib/libsqlcipher.so.0 lib/
-------------------------------------------------------------------------------------------------------------------
Instruction to Create fccmp library in lib/ folder 


step1: cd to fc-cmp directory and make
step2: The library is saved in lib/ folder

------------------------------------------------------------------------------------------------------------------
Instruction to build the seagate-pysed-python3

step1: go to folder seagate-pysed-python3 folder: cd seagate-pysed-python3/
step2: check if there is any build folder which contains the pysed libraries
step3: If exist the remove the folder: sudo rm -r build/
step4: Build the openseaChest Binaries: python3 setup.py opensea
step5: Build the pysed: python3 setup.py build
step6: We can see shared libraries in build folder

---------------------------------------------------------------------------------------------------------------------
**************************************Instruction to run the FCCMP C-API'***************************************
----------------------------------------------------------------------------------------------------------------------

-------------> KMIP Commands  
1.1) Adding p6kmiptool.conf and create database kmip config table 

step1: Go to test/ folder : cd test/
step2: Compile FCCMP C-API Tester : gcc testAddClientIdentity.c -I../src -L../lib/ -lfccmp -o testAddClientIdentity -lpython3.6m
step3: Run the API Tester: sudo LD_LIBRARY_PATH=../lib ./testAddClientIdentity 
step4: Output
{"STATUS": "Success", "ERROR": "None", "RESULT": "KMIP Server added to the configuration file and updated the database config table"}
step4: Check the encrypted database is created, Its created in /fccmpDB/ folder and its password is "testPassword"
     ---->  cd /fccmpDB/
     ---->  sqlcipher fccmpEncrypt.db
     sqlite> .tables
            Error: file is not a database
     sqlite> pragma key="testPassword";
     sqlite> .tables
            configDB
     sqlite> select * from configDB;
             1|False|False|None|client private key in PEM format|client public cert in PEM format|SERVER1|server1.domain.local|CA certificate              in PEM format|1.2

Step5: We can change the client data and recheck the table the data is appended
       sqlite> select * from configDB;
               1|False|False|None|client private key in PEM format|client public cert in PEM format|SERVER1|server1.domain.local|CA certificat               e in PEM format|1.2
               2|False|False|None|client private key in PEM format|client public cert in PEM format|SERVER2|server2.domain.local|CA certificat               e in PEM format|1.2
........................................................................
  Printing Database KMIP config file data

step1: Go to test folder : cd test/
step2: Compile FCCMP C-API Tester : gcc testPrintServerConfig.c -I../src -L../lib/ -lfccmp -o testPrintServerConfig -lpython3.6m
step3: Run the API Tester: sudo LD_LIBRARY_PATH=../lib ./testPrintServerConfig
step4: Output
{"STATUS": "Success", "ERROR": "None", "RESULT":[{"SERVER1": {"kmip_address": "server1.domain.local", "port": "5696", "reconnect": "0", "client_cert": "/clientCert/server1_cert.pem", "client_key": "/clientKey/server1_key.pem", "KMIPprotocol": "1.2", "nbio": "0", "timeout": "6", "KMIPCA_cert": "/serverCA/server1_CA.pem"}}, {"SERVER1": {"kmip_address": "server1.domain.local", "port": "5696", "reconnect": "0", "client_cert": "/clientCert/server1_cert.pem", "client_key": "/clientKey/server1_key.pem", "KMIPprotocol": "1.2", "nbio": "0", "timeout": "6", "KMIPCA_cert": "/serverCA/server1_CA.pem"}}]}

........................................................................
  Editing p6kmiptool.conf and create edit database kmip config table

step1: Go to test folder : cd test/
step2: Compile FCCMP C-API Tester : gcc testUpdateServerConfig.c -I../src -L../lib/ -lfccmp -o testUpdateServerConfig -lpython3.6m
step3: Check the already created encrypted database and p6kmiptool.conf file data and its table data before editing
     ---->  cd /fccmpDB/
     ---->  sqlcipher fccmpEncrypt.db
     sqlite> .tables
            Error: file is not a database
     sqlite> pragma key="testPassword";
     sqlite> .tables
            configDB
     sqlite> select * from configDB;
             1|False|False|None|client private key in PEM format|client public cert in PEM format|SERVER1|server1.domain.local|CA certificate              in PEM format|1.2

Step4: Now we will update the database and p6kmiptool.conf file. 
       sudo LD_LIBRARY_PATH=../lib ./testUpdateServerConfig
       output: {"STATUS": "Success", "ERROR": "None", "RESULT": "The Server config File and Database is updated"}
step5: Now check the p6kmiptool.conf file and database, server Address is changed.
       sqlite> select * from configDB;
               1|False|False|None|client private key in PEM format|client public cert in PEM format|SERVER2|server6.domain.local|CA certificate in PEM format|1.2
       
.....................................................................
 Deleting p6kmiptool.conf server Data and in database kmip config table

step1: Go to test folder : cd test/
step2: Compile FCCMP C-API Tester : gcc testDeleteServerConfig.c -I../src -L../lib/ -lfccmp -o testDeleteServerConfig -lpython3.6m
step3: Check the already created encrypted database and p6kmiptool.conf file data and its table data before editing
     ---->  cd /fccmpDB/
     ---->  sqlcipher fccmpEncrypt.db
     sqlite> .tables
            Error: file is not a database
     sqlite> pragma key="testPassword";
     sqlite> .tables
            configDB
     sqlite> select * from configDB;
             6|False|False|None|client private key in PEM format|client public cert in PEM format|SERVER2|server1.domain.local|CA certificate in PEM format|1.2

Step4: Now we will Delete the database and in p6kmiptool.conf file.
       sudo LD_LIBRARY_PATH=../lib ./testDeleteServerConfig
       output: {"STATUS": "Success", "ERROR": "None", "RESULT": "The Server config File Data is deleted from Database "}
step5: Now check the p6kmiptool.conf file and database, server Address is changed.
       sqlite> select * from configDB;

.......................................................................
1.3) Generate Client Credentials the csr and key data are saved in files in path /CNCSR

step1: Got to test folder : cd test/
step2: Compile FCCMP C-API Tester : gcc testGenerateClientCredentials.c -I../src -L../lib/ -lfccmp -o testGenerateClientCredentials -lpython3.       6m
step3: sudo LD_LIBRARY_PATH=../lib ./testGenerateClientCredentials
       output: {"STATUS": "Success", "ERROR": "None", "RESULT": {"CSRpath": "/CNCSR/johndoe.csr", "KeyPath": "/CNCSR/johndoe.key"}}
........................................................................
1.4) Show CSR of all common names or specific common name in path /CNCSR

step1: Got to test folder : cd test/
step2: Compile FCCMP C-API Tester : gcc testShowCSR.c -I../src -L../lib/ -lfccmp -o testShowCSR -lpython3.6m
step3: sudo LD_LIBRARY_PATH=../lib ./testShowCSR
       output: {"STATUS": "Success", "ERROR": "None", "RESULT": [{"CN": "johndoe", "CSR": "-----BEGIN CERTIFICATE REQUEST-----\nMIIC2TCCAcECAQAwgZMxETAPBgNVBAMMCEpvaG4gRG9lMQswCQYDVQQGEwJVUzEQ\nMA4GA1UECAwHR2VvcmdpYTEQMA4GA1UEBwwHQXRsYW50YTEYMBYGA1UECgwPQUJD\nIENvbXBhbnkgSW5jMRMwEQYDVQQLDApBY2NvdW50aW5nMR4wHAYJKoZIhvcNAQkB\nFg9qZG9lQGFiYy1jby5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIB\nAQC2S6n6mgOEqqIXRREALp/3X9hEwVtl2EkPYUefrQ/HXT91M/AIqKEd3MbhCD9U\nRgN9w5A8egO9LiOuR4rJ6Z2UuKMArKIKHsMMiDNdsgZN4Ot+BXIsaSeXn5LKiifK\neaNIG+LcxDUWuJGRqXvycPtgjc6kvtViwgSnowiLvHF2VHxLfLSY8p5GQ3FrQVOS\nEphHR8TL96PTukEmVttPB36XsBL9DFuPv2ZPVmiB9kk1vBpq4Vs/GJRmDhLjANeB\nTh9V65kFllzIdY5tuaC1I1j6+fXcbxSI7I6qRIEcFv3kynhyithpnyvnengMXH9w\ndTFdErbXdno27cOponc5vHirAgMBAAGgADANBgkqhkiG9w0BAQsFAAOCAQEAJKUN\n5QSLWys/Dz6Mppx91HjvQKHj26zcziIm5MKcrzGNWNDlyn6amRAzIftbKv7J7P0c\nH+uJ21kOM3Ckm3KkPcCyfERgcUyrFEK4pYfOauB82cpnOdREhigNg6MzsM/hB8Rq\nqmpwJF6gNA1gOaZV/OqD3C9TPf0Ed9pVocAuJpnWYv8SSdshmJ17dHdXh1Oya9/2\nNUqYL7Cc5YiLNppifwkB8r4kzAq+mFEd+vWp7E1h1MBsVkNmL+fy/A5KjHQaL5M9\ntRohFnVU2CPDdAir2bx2v/GGcaEWOYeLMi86gyY2QixanlsiRG0E6u9+1oNw8fLv\nqDd97777l6H0QOBidA==\n-----END CERTIFICATE REQUEST-----\n"}]}

--------------> TCG Drive Commands 
2.1)  Drive Scan using pysed openSeachest... In this TCG and FIPS_Avail is not implemented 

step1: Go to test folder : cd test/
step2: Compile FCCMP C-API Tester : gcc testDriveScanAPI.c -I../src -L../lib/ -lfccmp -o testDriveScanAPI -lpython3.6m
Step3: sudo LD_LIBRARY_PATH=../lib ./testDriveScanAPI
       output: {"STATUS": "Success", "ERROR": "None", "RESULT": [{"Vendor": "VBOX", "Device": "/dev/sg0", "Model_Number": "", "TCG": "Not Implemented", "FIPS_Avail": "Not Implemented"}, {"Vendor": "ATA", "Device": "/dev/sg1", "Model_Number": "VBc5fc91e9-cb0e869e", "TCG": "Not Implemented", "FIPS_Avail": "Not Implemented"}]}

..........................................................................
2.2)  Drive Query using pysed openSeachest. 

step1: Go to test folder : cd test/
step2: Compile FCCMP C-API Tester. If we want we can change the Device name in code and recompile : gcc testDriveQuery.c -I../src -L../lib/ -l       fccmp -o testDriveQuery -lpython3.6m
Step3: sudo LD_LIBRARY_PATH=../lib ./testDriveQuery
       output: {"STATUS": "Success", "ERROR": "None", "RESULT": {"modelNumber": "WDC WD2500BEVT-75ZCT2", "serialNumber": "WD-WXEX08PXF141", "firmwareRevision": "11.01A11", "worldWideName": "50014EE0564BDA7A", "driveCapacity(GB/GiB)": "250.06/232.89", "nativeDriveCapacity": "250.06/232.89", "temperatureData": {"currentTemperature": "51", "highestTemperature": "62", "lowestTemperature": "0"}, "powerOnTime": {"years": "0", "days": "250", "hours": "6", "minutes": "0", "seconds": "0"}, "powerOnHours": "6006.00", "maxLBA": "488397167", "nativeMaxLBA": "488397167", "logicalSectorSize": "512", "physicalSectorSize": "512", "sectorAlignment": "0", "rotationRate": "5400", "formFactor": "Not Reported", "lastDSTinformation": {"timeSinceLastDST(hours)": "117.00", "DSTstatus": "0x2", "DSTTestRun": "0x0"}, "longDSTtestTime": {"years": "0", "days": "0", "hours": "1", "minutes": "30", "seconds": "0"}, "interfaceSpeed": {"maxSpeed(Gb/s)": "3.0", "negotiatedSpeed(Gb/s)": "Not Reported"}, "annualizedWorkloadRate(TB/yr)": "Not Reported", "totalBytesRead": "Not Reported", "totalBytesWritten": "Not Reported", "encryptionSupport": "Not Reported", "cacheUnit": "MiB", "cacheSize": "8.00", "readLookAhead": "Enabled", "writeCache": "Enabled", "smartStatus": "Good", "ataSecurityInformation": "Supported, Frozen", "specificationsSupported": "ATA8-ACS ,ATA/ATAPI-7 ,ATA/ATAPI-6 ,ATA/ATAPI-5 ,ATA/ATAPI-4 ,ATA-3 ,ATA-2 ,ATA-1 ,SATA 2.5 ,SATA II: Extensions ,SATA 1.0a ,", "featuresSupported": "SATA NCQ ,SATA Software Settings Preservation [Enabled] ,SATA Device Initiated Power Management ,HPA ,Power Management ,Security ,SMART [Enabled] ,DCO ,48bit Address ,AAM ,APM [Enabled] ,GPL ,SMART Self-Test ,SMART Error Logging ,SCT Read/Write Long ,SCT Write Same ,SCT Error Recovery Control ,SCT Feature Control ,SCT Data Tables ,Host Logging ,"}}

........................................................................
2.3)  Tcg Info using pysed openSeachest, currently using pre build binary.

step1: Go to test folder : cd test/
step2: Compile FCCMP C-API Tester : gcc testTcgInfo.c -I../src -L../lib/ -lfccmp -o testTcgInfo -lpython3.6m
Step3: sudo LD_LIBRARY_PATH=../lib ./testTcgInfo

........................................................................
2.4) Drive Status all, TCG, FIPS , Data Protection and Managed State are not implemented

step1: Go to test folder : cd test/
step2: Compile FCCMP C-API Tester : gcc testDriveStatus.c -I../src -L../lib/ -lfccmp -o testDriveStatus -lpython3.6m
Step3: sudo LD_LIBRARY_PATH=../lib ./testDriveStatus
       output: {"STATUS": "Success", "ERROR": "None", "RESULT": [{"Device": "/dev/sg0", "Model_Number": "", "FiPS_Avail": "Not Supported", "TCG": "Not Supported", "DataProtection": "Not Implemented", "ManagedState": "Not Implemented"}, {"Device": "/dev/sg1", "Model_Number": "WD-WXEX08PXF141", "FiPS_Avail": "Not Supported", "TCG": "Not Supported", "DataProtection": "Not Implemented", "ManagedState": "Not Implemented"}]}

----------------------------------------------------------------------------------------------

