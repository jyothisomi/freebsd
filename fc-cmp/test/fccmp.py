#!/usr/bin/env python3
# FCDMS Application

import json
import sys

sys.path.insert(0, '../utils')
from KMIP_Utils import addClientIdentity,printKMIPConfig,updateKMIPServer,generateClientCredential,printCSR,queryServer,verifyServer,checkStatus,configBatchMode,deleteKMIPServer
from TCGDrive_Utils import getDriveData,getDrivePinData,scanDrives,driveQuery,tcgInfo,driveStatus
from DriveManagement_Utils import deviceConfigure,driveOwnership,changeTcgPin,fipsMode,getPort,setFirmwarePort
from Authentication_Utils import displayMSID,displayDriveTable,checkPINS,changePINS
from Locking_Utils import activateLockingRanges,activateLockingRange,setStatusLockingRange,setRange,manageDriveEnable,unlockRanges,changeLockingPINS,getLockingRanges,eraseRange
from DriveRevert_Utils import eraseDrive,disableAuthority,revertPSID

#*****************************************************************

#---Inf: Runs the function according to command received
#---Par: function command and their arguments
#---Ret: function to be executed
def sendCommand(command, args):
    
    if args:
        arguments = json.loads(args)
    else:
        arguments = ""

    switcher={
    	"scan": scanDrives,
        "driveData": getDriveData,
        "drivePinData": getDrivePinData,
        "addClient": lambda: addClientIdentity(arguments),
        "printKMIPConfig": printKMIPConfig,
        "updateKMIPServer": lambda: updateKMIPServer(arguments),
        "deleteKMIPServer": lambda: deleteKMIPServer(arguments),
        "generateClientCredential": lambda: generateClientCredential(arguments),
        "printCSR": lambda: printCSR(arguments),
        "queryServer": lambda: queryServer(arguments),
        "verifyServer": lambda: verifyServer(arguments),
        "checkStatus": checkStatus,
        "configBatchMode": lambda: configBatchMode(arguments),
        "driveQuery" : lambda: driveQuery(arguments),
        "tcgInfo" : lambda: tcgInfo(arguments),
        "driveStatus" : driveStatus,
        "deviceConfigure" : lambda: deviceConfigure(arguments),
        "driveOwnership" : lambda: driveOwnership(arguments),
        "changeTcgPin" : lambda: changeTcgPin(arguments),
        "fipsMode" : lambda: fipsMode(arguments),
        "getPort" : lambda: getPort(arguments),
        "setFirmwarePort" : lambda: setFirmwarePort(arguments),
        "displayMSID" : lambda: displayMSID(arguments),
        "displayDriveTable" : lambda: displayDriveTable(arguments),
        "checkPINS" : lambda: checkPINS(arguments),
        "changePINS" : lambda: changePINS(arguments),
        "activateLockingRanges" : lambda: activateLockingRanges(arguments),
        "activateLockingRange" : lambda: activateLockingRange(arguments),
        "setStatusLockingRange" : lambda: setStatusLockingRange(arguments),
        "setRange" : lambda: setRange(arguments),
        "manageDriveEnable" : lambda: manageDriveEnable(arguments),
        "unlockRanges" : unlockRanges,
        "changeLockingPINS" : lambda: changeLockingPINS(arguments),
        "getLockingRanges": lambda: getLockingRanges(arguments),
        "eraseRange" : lambda: eraseRange(arguments),
        "eraseDrive" : lambda: eraseDrive(arguments),
        "disableAuthority" : lambda: disableAuthority(arguments),
        "revertPSID" : lambda: revertPSID(arguments)
    	}
    #print (command)
    func = switcher.get(command, lambda :"[]")
    return func()

#*****************************************************************

