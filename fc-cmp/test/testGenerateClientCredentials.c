#include "fccmp.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(void){

	char *response;
        char *configData;
	configData = "{\"type\":\"client\", \"country\":\"US\", \"state\":\"Georgia\", \"locality\":\"Atlanta\",\"organization\":\"ABC Company Inc\", \"unit\":\"Accounting\", \"name\":\"Paul Greg\",\"email\":\"jdoe@abc-co.com\", \"keysize\":2048, \"encryption\":\"rsa\"}";
        response = generateClientCredential(configData);
        printf("%s\n",response);
        return 0;

}
