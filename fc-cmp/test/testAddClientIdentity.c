#include "fccmp.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(void){

	char *response;
        char *configData;
	configData = "{ \"kmip_servername\": \"SERVER2\",\"kmip_address\": \"server2.domain.local\", \"client_key\":\"client private key in PEM format\", \"client_cert\":\"client public cert in PEM format\", \"KMIPCA_cert\":\"CA certificate in PEM format\", \"KMIPprotocol\":\"1.2\"}";
        response = addClientIdentity(configData);
        printf("%s\n",response);
        return 0;

}
