#ifndef _FC_DMS_H_
#define _FC_DMS_H_


#define FCDMS_SUCCESS 0

typedef struct drive_status{
	char UUID[3];
	char SN[16];
	char DevicePath[16];
	char TCGSupport[8];
	char LockedStatus[8];
	char FIPSSupport[5];
	char FIPSMode[16];
	char BootDrive[5];
	char LockingRanges[16];
}DriveStatus;

int getDriveData(DriveStatus** );
char *addClientIdentity(char *);
char *printKMIPConfig();
char *updateKMIPServer(char *);
char *deleteKMIPServer(char *);
char *generateClientCredential(char *);
char *getCSR(char *);
char *queryServer(char *);
char *verifyServer(char *);
char* checkStatus();
char *configBatchMode(char *);
char *driveScan();
char *driveQuery(char *);
char *driveStatus();
char *deviceConfigure(char *);
char *tcgInfo(char *);
char *driveOwnership(char *);
char *changeTcgPin(char *);
char *FIPSmode(char *);
char *driveDataTable();
char *drivePinTable();
char *getPort(char *);
char *setFirmwarePort(char *);
char *displayMSID(char *);
char *displayDriveTable(char *);
char *checkPINS(char *);
char *changePINS(char *);
char *activateLockingRanges(char *);
char *activateLockingRange(char *);
char *setStatusLockingRange(char *);
char *setRange(char *);
char *manageDriveEnable(char *);
char *unlockRanges();
char *changeLockingPINS(char *);
char *getLockingRanges(char *);
char *eraseRange(char *);
char *eraseDrive(char *);
char *disableAuthority(char *);
char *revertPSID(char *);
#endif
