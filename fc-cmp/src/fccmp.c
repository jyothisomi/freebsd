#include "interface.h"
#include "fccmp.h"
#include "defs.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>


char *addClientIdentity(char *configData){
	char *result;
	result = sendCommand(FCDMS_ADD_CLIENT,configData);
        return result;

}
char *printKMIPConfig(){
        char *result;
        result = sendCommand(FCDMS_PRINT_KMIP_CONFIG, "{}");
        return result;


}
char *updateKMIPServer(char *configData){
	char *result;
        result = sendCommand(FCDMS_UPDATE_KMIP_SERVER,configData);
        return result;
}
char *deleteKMIPServer(char *configData){
	char *result;
        result = sendCommand(FCDMS_DELETE_KMIP_SERVER,configData);
        return result;
}
char *generateClientCredential(char *csrData){
	char *result;
        result = sendCommand(FCDMS_GENERATE_CLIENT,csrData);
        return result;

}
char *getCSR(char *cn){
	char *result;
        result = sendCommand(FCDMS_PRINT_CSR,cn);
        return result;
	

}
char *queryServer(char *query){
	char *result;
        result = sendCommand(FCDMS_QUERY_SERVER,query);
        return result;

}
char *verifyServer(char *query){
	char *result;
        result = sendCommand(FCDMS_VERIFY_SERVER,query);
        return result;
}
char* checkStatus(){
	char *result;
        result = sendCommand(FCDMS_CHECK_STATUS, "{}");
        return result;

}
char *configBatchMode(char *Mode){
	char *result;
        result = sendCommand(FCDMS_BATCH_MODE,Mode);
        return result;
}
char *driveScan(){
	char *result;
        result = sendCommand(FCDMS_DRIVE_SCAN, "{}");
        return result;
}
char *driveQuery(char *drive){
	char *result;
        result = sendCommand(FCDMS_Drive_QUERY,drive);
        return result;
}
char *tcgInfo(char *drive){
        char *result;
        result = sendCommand(FCDMS_TCG_INFO,drive);
        return result;
}
char *driveStatus(){
	char *result;
        result = sendCommand(FCDMS_DRIVE_STATUS, "{}");
        return result;
}
char *deviceConfigure(char *configData){
	char *result;
        result = sendCommand(FCDMS_DEVICE_CONFIGURE,configData);
        return result;
}
char *driveOwnership(char *data){
	char *result;
        result = sendCommand(FCDMS_DRIVE_OWNERSHIP,data);
        return result;
}
char *changeTcgPin(char *data){
	char *result;
        result = sendCommand(FCDMS_CHANGE_TCGPIN,data);
        return result;
}
char *FIPSmode(char *data){
	char *result;
        result = sendCommand(FCDMS_FIPS_MODE,data);
        return result;
}
char *driveDataTable(){
        char *result;
        result = sendCommand(FCDMS_DRIVE_DATA_TABLE, "{}");
        return result;
}
char *drivePinTable(){
        char *result;
        result = sendCommand(FCDMS_DRIVE_PIN_TABLE, "{}");
        return result;
}
char *getPort(char *drive){
	char *result;
        result = sendCommand(FCDMS_GET_PORT,drive);
        return result;
}
char *setFirmwarePort(char *data){
	char *result;
        result = sendCommand(FCDMS_SET_FIRMWARE_PORT,data);
        return result;

}
char *displayMSID(char *drive){
	char *result;
        result = sendCommand(FCDMS_DISPLAY_MSID,drive);
        return result;
}
char *displayDriveTable(char * data){
	char *result;
        result = sendCommand(FCDMS_DISPLAY_DRIVE_TABLE,data);
        return result;
}
char *checkPINS(char *drive){
	char *result;
        result = sendCommand(FCDMS_CHECK_PINS,drive);
        return result;
}
char *changePINS(char *drive){
	char *result;
        result = sendCommand(FCDMS_CHANGE_PINS,drive);
        return result;
}
char *activateLockingRanges(char *data){
	char *result;
        result = sendCommand(FCDMS_ACTIVATE_LOCK_RANGES,data);
        return result;
}
char *activateLockingRange(char *data){
	char *result;
        result = sendCommand(FCDMS_ACTIVATE_LOCK_RANGE,data);
        return result;
}
char *setStatusLockingRange(char *data){
	char *result;
        result = sendCommand(FCDMS_STATUS_LOCK_RANGE,data);
        return result;
}
char *setRange(char *data){
	char *result;
        result = sendCommand(FCDMS_SET_RANGE,data);
        return result;
}
char *manageDriveEnable(char *data){
	char *result;
        result = sendCommand(FCDMS_MANAGE_DRIVE,data);
        return result;
}
char *unlockRanges(){
	char *result;
        result = sendCommand(FCDMS_UNLOCK_RANGES,"{}");
        return result;
}
char *changeLockingPINS(char *data){
	char *result;
        result = sendCommand(FCDMS_CHANGE_LOCKING_PINS,data);
        return result;
}
char *getLockingRanges(char *data){
	char *result;
        result = sendCommand(FCDMS_GET_LOCKING_RANGES,data);
        return result;
}
char *eraseRange(char *data){
	char *result;
        result = sendCommand(FCDMS_ERASE_RANGE,data);
        return result;
}
char *eraseDrive(char *drive){
	char *result;
        result = sendCommand(FCDMS_ERASE_DRIVE,drive);
        return result;
}
char *disableAuthority(char *data){
	char *result;
        result = sendCommand(FCDMS_DISABLE_AUTHORITY,data);
        return result;
}
char *revertPSID(char *drive){
	char *result;
        result = sendCommand(FCDMS_REVERT_PSID,drive);
        return result;
}



