//#include <python3.6m/Python.h>
#include <python3.6m/Python.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
char* sendCommand(char *cmd, char* args)
{
  // Set PYTHONPATH TO working directory
   setenv("PYTHONPATH",".",1);

   PyObject *pName, *pModule, *pDict, *pFunc,  *pValue, *pResult;
   //PyObject *pModule, *pDict, *pFunc,  *pValue, *presult;
   char* ret = NULL;
   char* error = "ERROR";
   int i;


   // Initialize the Python Interpreter
   Py_Initialize();

   // Build the name object
   pName = PyUnicode_DecodeFSDefault("fccmp");  // For python 3
   //pName = pName = PyString_FromString((char*)"fcdms"); // For python 2
   // Load the module object
   pModule = PyImport_Import(pName);
   Py_DECREF(pName);

   if (pModule != NULL){

	pFunc = PyObject_GetAttrString(pModule, "sendCommand");
	if(pFunc && PyCallable_Check(pFunc)){
		 pValue=Py_BuildValue("(zz)",cmd, args);
		 if (!pValue){
		     Py_DECREF(pModule);
		     Py_DECREF(pValue);
                     fprintf(stderr, "Cannot read argument\n");
		     return error;
		 }
		 pResult=PyObject_CallObject(pFunc, pValue);
		 Py_DECREF(pValue);
                 if (pResult != NULL){
			PyObject* str = PyUnicode_AsEncodedString(pResult, "utf-8", NULL);
                    	char *result = PyBytes_AsString(str);
		     ret = strdup(result); 
		     
		     Py_XDECREF(str);
		     Py_DECREF(pResult);    
	         }else{

	  	 	Py_DECREF(pFunc);
			Py_DECREF(pModule);
			PyErr_Print();
			fprintf(stderr, "Call failed\n");
			return error;
		 }

	}else{

		if (PyErr_Occurred())
			PyErr_Print();
		fprintf(stderr, "Cannot find function sendCommand");
	
	}

	Py_XDECREF(pFunc);
	Py_DECREF(pModule);

    }else{
    	
	PyErr_Print();
	fprintf(stderr, "Failed to load fcdms.py");
	return error;

    }
    // for python 3
    if (Py_FinalizeEx() < 0){    
        return error;
    }
    // for python 2
    //Py_Finalize();

   return ret;
}
