#!/usr/bin/env python3

#---------Funtion API's used by fcdms application

import json
import sys
import os


sys.path.insert(0, '../seagate-pysed-python3/build/lib.linux-x86_64-3.6')
from sed_enablement.tcgapi import Sed,fcdmsSed

#************************************************************************
#---Inf: Get the MSID value
#---Par: drive details
#---Ret: True/False and MSID Value
def getdisplayMSID(arguments):
    try:
            #Process to generate CSR
            #Data = "Server Query Data"

            msidData = {"MSID": "6EFBFE5C35BC0D54407351F81918F605"}
            return True,msidData
    except:
        return False,None
#************************************************************************

#---Inf: Get the drive MSID value
#---Par: user arguments from panansas application
#---Ret: Status of command execution and result in json
def displayMSID(arguments):
    status = ""
    error = ""
    result = ""

    ret,resp = getdisplayMSID(arguments)

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in getting the MSID Value "
        result= ''

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    return json.dumps(resp)
#************************************************************************

#---Inf: get the Drive Tables
#---Par: Table type and drive
#---Ret: True/False and table data 
def getDisplayDriveTable(arguments):
    tableType = None
    drive = None
    tableType = arguments["type"]
    drive = arguments["drive"]

    try:
      if drive:
         if "admin" in tableType:
            #Process to generate CSR
            #Data = "Server Query Data"

            serData = [{"Authority": "SID", "Status":"Enabled", "KeySource": "KMIP","Key ID": "3481d00d-86f3-435e-bfc4-7d201d317936"},{"Authority": "Maker", "Status":"Enabled", "KeySource": "C_PIN Table","Key ID": "NA"},{"Authority": "Anybody", "Status":"Enabled", "KeySource": "C_PIN Table","Key ID": "NA"}]
            return True,serData
         elif "locking" in tableType:
            serData = [{"Authority": "Anybody", "Status": "Enabled", "KeySource": "C_PINTable", "Key ID": "NA"},{"Authority": "BandMaster0", "Status": "Enabled", "KeySource": "KMIP", "Key ID": "3481d00d-86f3-435e-bfc4-7d201d317936"},{"Authority": "EraseMaster", "Status": "Enabled", "KeySource": "C_PINKMIP", "Key ID": "db44344e-afb9-4067-ba67-f94581c544c2"},{"Authority": "BandMaster1", "Status": "Enabled", "KeySource": "KMIP", "Key ID": "b16be6e8-0d3d-42b3-a15e-81e738f68903"}]
            return True,serData
         else:
            return False,None
    except:
        return False,None
#************************************************************************

#---Inf: Display the Admin or Locking Drive Table 
#---Par: user arguments from panansas application
#---Ret: Status of command execution and result in json
def displayDriveTable(arguments):
    status = ""
    error = ""
    result = ""

    ret,resp = getDisplayDriveTable(arguments)

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in getting the Drive Tables "
        result= ''

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    return json.dumps(resp)
#************************************************************************

#---Inf: get the Pins validity
#---Par: drive
#---Ret: True/False and PINS detail
def getCheckPINS(arguments):
    drive = None
    drive = arguments["drive"]

    try:
      if drive:
         if "all" in drive:
            #Process to generate CSR
            #Data = "Server Query Data"

            serData = [{"Drive":"/dev/sda", "Authority": "SID", "Sources":"KMIP","PIN_Status": "Valid"},{"Drive":"/dev/sda", "Authority": "BandMaster0", "Sources":"KMIP","PIN_Status": "Valid"},{"Drive":"/dev/sda", "Authority": "EraseMaster", "Sources":"KMIP","PIN_Status": "Valid"},{"Drive":"/dev/sda", "Authority": "BandMaster1", "Sources":"KMIP","PIN_Status": "Valid"},{"Drive":"/dev/sdb", "Authority": "SID", "Sources":"KMIP","PIN_Status": "Valid"},{"Drive":"/dev/sdb", "Authority": "BandMaster0", "Sources":"KMIP","PIN_Status": "Valid"},{"Drive":"/dev/sdb", "Authority": "EraseMaster", "Sources":"KMIP","PIN_Status": "Valid"},{"Drive":"/dev/sdb", "Authority": "BandMaster1", "Sources":"KMIP","PIN_Status": "Valid"}]
            return True,serData
         else:
            serData =  [{"Authority": "SID", "Sources":"KMIP","PIN_Status": "Valid"},{"Authority": "BandMaster0", "Sources":"KMIP","PIN_Status": "Valid"},{"Authority": "EraseMaster", "Sources":"KMIP","PIN_Status": "Valid"},{"Authority": "BandMaster1", "Sources":"KMIP","PIN_Status": "Valid"}]
            return True,serData
         
    except:
        return False,None
#************************************************************************


#---Inf: Validate Pins for authority 
#---Par: user arguments from panansas application
#---Ret: Status of command execution and result in json
def checkPINS(arguments):
    status = ""
    error = ""
    result = ""

    ret,resp = getCheckPINS(arguments)

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in getting the Validate Pins for Authority "
        result= ''

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    return json.dumps(resp)
#************************************************************************
#---Inf: change the Pins credentials
#---Par: drive
#---Ret: True/False and change pins status
def getChangePINS(arguments):
    drive = None
    drive = arguments["drive"]

    try:
      if drive:
         if "all" in drive:
            #Process to generate CSR
            #Data = "Server Query Data"

            serData = [{"Drive":"/dev/sda", "Authority": "BandMaster0","Status":"success"},{"Drive":"/dev/sda", "Authority": "EraseMaster", "Status":"success"},{"Drive":"/dev/sda", "Authority": "BandMaster1", "Status":"success"},{"Drive":"/dev/sdb", "Authority": "BandMaster0", "Status":"success"},{"Drive":"/dev/sdb", "Authority": "EraseMaster", "Status":"success"},{"Drive":"/dev/sdb", "Authority": "BandMaster1", "Status":"success"}]
            return True,serData
         else:
            serData = [{"Authority": "BandMaster0", "Status":"success"},{"Authority": "EraseMaster", "Status":"success"},{"Authority": "BandMaster1", "Status":"success"}] 
            return True,serData

    except:
        return False,None
#************************************************************************


#---Inf: Change Pins for authority 
#---Par: user arguments from panansas application
#---Ret: Status of command execution and result in json
def changePINS(arguments):
    status = ""
    error = ""
    result = ""

    ret,resp = getChangePINS(arguments)

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in changing Pins credentials for Authority "
        result= ''

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    return json.dumps(resp)

