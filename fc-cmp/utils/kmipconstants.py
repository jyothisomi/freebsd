import enum

# MIP protocol constants (e.g., ALG_AES for the KMIP constant for the AES symmetric key cipher)
#
# Copyright P6R Inc. - Please see the included copyright.txt file for details.
# Date 06/19/2016
#
# Section 9.1.3 Define Values from the KMIP Specification
#
# Several of the functions in the p6pythinkmip extension require the standard KMIP
# constant values as parameters.   For example, for the function p6pythinkmip.createSymmetricKey() has
# the parameter "cryptoAlg" that needs to be set from a value of ALG_XXX (e.g., ALG_AES, cryptographic algorithm AES).
# The reason we use constants for these enumerations is because KMIP allows extension values to be used as integer
# values of the form: 0x8XXXXXXX  for all these parameters.
#

# Recommended Curve Enumeration (KMIP Spec Section 9.1.3.2.5)
#
class RecommendedCurveEnumeration(enum.IntEnum):
    CURVE_P192 = 0x0001 
    CURVE_K163 = 0x0002 
    CURVE_B163 = 0x0003 
    CURVE_P224 = 0x0004 
    CURVE_K233 = 0x0005 
    CURVE_B233 = 0x0006 
    CURVE_P256 = 0x0007 
    CURVE_K283 = 0x0008 
    CURVE_B283 = 0x0009 
    CURVE_P384 = 0x000A 
    CURVE_K409 = 0x000B 
    CURVE_B409 = 0x000C 
    CURVE_P521 = 0x000D 
    CURVE_K571 = 0x000E 
    CURVE_B571 = 0x000F 
    CURVE_SECP112R1 = 0x0010 
    CURVE_SECP112R2 = 0x0011 
    CURVE_SECP128R1 = 0x0012 
    CURVE_SECP128R2 = 0x0013 
    CURVE_SECP160K1 = 0x0014 
    CURVE_SECP160R1 = 0x0015 
    CURVE_SECP160R2 = 0x0016 
    CURVE_SECP192K1 = 0x0017 
    CURVE_SECP224K1 = 0x0018 
    CURVE_SECP256K1 = 0x0019 
    CURVE_SECT113R1 = 0x001A 
    CURVE_SECT113R2 = 0x001B 
    CURVE_SECT131R1 = 0x001C 
    CURVE_SECT131R2 = 0x001D 
    CURVE_SECT163R1 = 0x001E 
    CURVE_SECT193R1 = 0x001F 
    CURVE_SECT193R2 = 0x0020 
    CURVE_SECT239K1 = 0x0021 
    CURVE_ANSIX9P192V2 = 0x0022 
    CURVE_ANSIX9P192V3 = 0x0023 
    CURVE_ANSIX9P239V1 = 0x0024 
    CURVE_ANSIX9P239V2 = 0x0025 
    CURVE_ANSIX9P239V3 = 0x0026 
    CURVE_ANSIX9C2PNB163V1 = 0x0027 
    CURVE_ANSIX9C2PNB163V2 = 0x0028 
    CURVE_ANSIX9C2PNB163V3 = 0x0029 
    CURVE_ANSIX9C2PNB176V1 = 0x002A 
    CURVE_ANSIX9C2TNB191V1 = 0x002B 
    CURVE_ANSIX9C2TNB191V2 = 0x002C 
    CURVE_ANSIX9C2TNB191V3 = 0x002D 
    CURVE_ANSIX9C2PNB208W1 = 0x002E 
    CURVE_ANSIX9C2TNB239V1 = 0x002F 
    CURVE_ANSIX9C2TNB239V2 = 0x0030 
    CURVE_ANSIX9C2TNB239V3 = 0x0031 
    CURVE_ANSIX9C2PNB272W1 = 0x0032 
    CURVE_ANSIX9C2PNB304W1 = 0x0033 
    CURVE_ANSIX9C2TNB359V1 = 0x0034 
    CURVE_ANSIX9C2PNB368W1 = 0x0035 
    CURVE_ANSIX9C2TNB431R1 = 0x0036 
    CURVE_BRAINPOOL_P160R1 = 0x0037 
    CURVE_BRAINPOOL_P160T1 = 0x0038 
    CURVE_BRAINPOOL_P192R1 = 0x0039 
    CURVE_BRAINPOOL_P192T1 = 0x003A 
    CURVE_BRAINPOOL_P224R1 = 0x003B 
    CURVE_BRAINPOOL_P224T1 = 0x003C 
    CURVE_BRAINPOOL_P256R1 = 0x003D 
    CURVE_BRAINPOOL_P256T1 = 0x003E 
    CURVE_BRAINPOOL_P320R1 = 0x003F 
    CURVE_BRAINPOOL_P320T1 = 0x0040 
    CURVE_BRAINPOOL_P384R1 = 0x0041 
    CURVE_BRAINPOOL_P384T1 = 0x0042 
    CURVE_BRAINPOOL_P512R1 = 0x0043 
    CURVE_BRAINPOOL_P512T1 = 0x0044 


# Digital Signature Algorithm Enumeration (KMIP Spec Section 9.1.3.2.7)
#
class SignatureAlgorithmEnumeration(enum.IntEnum):
    SIG_MD2RSA      = 0x0001    # MD2 with RSA Encryption (PKCS#1 v1.5)
    SIG_MD5RSA      = 0x0002    # MD5 with RSA Encryption (PKCS#1 v1.5)
    SIG_SHA1RSA     = 0x0003    # SHA-1 RSA Encryption    (PKCS#1 v1.5)
    SIG_SSH224RSA   = 0x0004    # SHA-224 with RSA Encryption (PKCS#1 v1.5)
    SIG_SHA256RSA   = 0x0005    # SHA-256 with RSA Encryption (PKCS#1 v1.5)
    SIG_SHA384RSA   = 0x0006    # SHA-384 with RSA Encryption (PKCS#1 v1.5)
    SIG_SHA512RSA   = 0x0007    # SHA-512 with RSA Encryption (PKCS#1 v1.5)
    SIG_RSASSA      = 0x0008    # RSASSA-PS (PKCS#1 v2.1)
    SIG_DSASHA1     = 0x0009    # DSA with SHA-1
    SIG_DSASHA224   = 0x000A    # DSA with SHA224
    SIG_DSASHA256   = 0x000B    # DSA with SHA256
    SIG_ECDSASHA1   = 0x000C    # ECDSA with SHA-1
    SIG_ECDSASHA224 = 0x000D    # ECDSA with SHA224
    SIG_ECDSASHA256 = 0x000E    # ECDSA with SHA256
    SIG_ECDSASHA384 = 0x000F    # ECDSA with SHA384
    SIG_ECDSASHA512 = 0x0010    # ECDSA with SHA512

	
# Secret Data Type Enumeration (KMIP Spec Section 9.1.3.2.9)
#
class SecretDataTypeEnumeration(enum.IntEnum):
    SECRET_PASSWORD = 0x0001
    SECRET_SEED     = 0x0002

	
# Object Type Enumeration (KMIP Spec Section 9.1.3.2.12)
#
class ObjectTypeEnumeration(enum.IntEnum):
    OBJECT_CERTIFICATE  = 0x0001 
    OBJECT_SYMMETRICKEY = 0x0002 
    OBJECT_PUBLICKEY    = 0x0003 
    OBJECT_PRIVATEKEY   = 0x0004 
    OBJECT_SPLITKEY     = 0x0005 
    OBJECT_TEMPLATE     = 0x0006 
    OBJECT_SECRETDATA   = 0x0007 
    OBJECT_OPAQUE       = 0x0008 
    OBJECT_PGPKEY       = 0x0009 
	
	
# Cryptographic Algorithm Enumeration (KMIP Spec Section 9.1.3.2.13)
#	
class CryptographicAlgorithmEnumeration(enum.IntEnum):
    ALG_DES         = 0x0001 
    ALG_DES3        = 0x0002 
    ALG_AES         = 0x0003 
    ALG_RSA         = 0x0004 
    ALG_DSA         = 0x0005 
    ALG_ECDSA       = 0x0006 
    ALG_HMAC_SHA1   = 0x0007 
    ALG_HMAC_SHA224 = 0x0008 
    ALG_HMAC_SHA256 = 0x0009 
    ALG_HMAC_SHA384 = 0x000A 
    ALG_HMAC_SHA512 = 0x000B 
    ALG_HMAC_MD5    = 0x000C 
    ALG_DH          = 0x000D 
    ALG_ECDH        = 0x000E 
    ALG_ECMQV       = 0x000F 
    ALG_BLOWFISH    = 0x0010 
    ALG_CAMELLIA    = 0x0011 
    ALG_CAST5       = 0x0012 
    ALG_IDEA        = 0x0013 
    ALG_MARS        = 0x0014 
    ALG_RC2         = 0x0015 
    ALG_RC4         = 0x0016 
    ALG_RC5         = 0x0017 
    ALG_SKIPJACK    = 0x0018 
    ALG_TWOFISH     = 0x0019 
    ALG_EC          = 0x001A 
    ALG_ONETIMEPAD  = 0x001B 
	
	
# Block Cipher Mode Enumeration (KMIP Spec Section 9.1.3.2.14)
#
class CipherModeEnumeration(enum.IntEnum):
    MODE_CBC         = 0x0001
    MODE_ECB         = 0x0002
    MODE_PCBC        = 0x0003
    MODE_CFB         = 0x0004
    MODE_OFB         = 0x0005
    MODE_CTR         = 0x0006
    MODE_CMAC        = 0x0007
    MODE_CCM         = 0x0008
    MODE_GCM         = 0x0009
    MODE_CBC_MAC     = 0x000A
    MODE_XTS         = 0x000B
    MODE_AESKEYWRAP  = 0x000C            # AESKeyWrapPadding
    MODE_NISTKEYWRAP = 0x000D
    MODE_AESKW       = 0x000E            # X9.102
    MODE_TDKW        = 0x000F            # X9.102
    MODE_AKW1        = 0x0010            # X9.102
    MODE_AKW2        = 0x0011            # X9.102


# Padding Method Enumeration (KMIP Spec Section 9.1.3.2.15)
#
class PaddingMethodEnumeration(enum.IntEnum):
    PAD_NONE  = 0x0001
    PAD_OAEP  = 0x0002
    PAD_PKCS5 = 0x0003
    PAD_SSL3  = 0x0004
    PAD_ZEROS = 0x0005
    PAD_ANSI  = 0x0006      # X9.23
    PAD_ISO   = 0x0007      # 10126
    PAD_PKCS1 = 0x0008      # v1.5
    PAD_X9_31 = 0x0009
    PAD_PSS   = 0x000A

 
# Hashing Algorithm Enumeration (KMIP Spec Section 9.1.3.2.16)
#
class HashingAlgorithmEnumeration(enum.IntEnum):
    HASH_MD2        = 0x0001
    HASH_MD4        = 0x0002
    HASH_MD5        = 0x0003
    HASH_SHA1       = 0x0004
    HASH_SHA224     = 0x0005
    HASH_SHA256     = 0x0006
    HASH_SHA384     = 0x0007
    HASH_SHA512     = 0x0008
    HASH_RIPEMD160  = 0x0009
    HASH_TIGER      = 0x000A
    HASH_WHIRLPOOL  = 0x000B
    HASH_SHA512_224 = 0x000C
    HASH_SHA512_256 = 0x000D

   
# Revocation Reason Code Enumeration (KMIP Spec Section 9.1.3.2.19)
# 
class RevocationReasonCodeEnumeration(enum.IntEnum):
    REVOCATION_UNSPECIFIED            = 0x000001
    REVOCATION_KEYCOMPROMISE          = 0x000002
    REVOCATION_CACOMPROMISE           = 0x000003
    REVOCATION_AFFILIATION_CHANGED    = 0x000004
    REVOCATION_SUPERSEDED             = 0x000005
    REVOCATION_CESSATION_OF_OPERATION = 0x000006
    REVOCATION_PRIVILEGE_WITHDRAWN    = 0x000007

    
# Link Type Enumeration (KMIP Spec Section 9.1.3.2.20 )
#
class LinkTypeEnumeration(enum.IntEnum):
    LINK_CERTIFICATE    = 0x0101
    LINK_PUBLICKEY      = 0x0102
    LINK_PRIVATEKEY     = 0x0103
    LINK_DERIVATION     = 0x0104       # Derivation Base Object Link
    LINK_DERIVEDKEY     = 0x0105
    LINK_REPLACEMENT    = 0x0106       # Replacement Object Link
    LINK_REPLACED       = 0x0107       # Replaced Object Link
    LINK_PARENT         = 0x0108
    LINK_CHILD          = 0x0109
    LINK_PREVIOUS       = 0x010A
    LINK_NEXT           = 0x010B
    LINK_PKCS12CERT     = 0x010C       # PKCS#12 Certificaate Link
    LINK_PKCS12PASSWORD = 0x010D       # PKCS#12 Password Link

	
# Derivation Method Enumeration (SKMIP Spec Section 9.1.3.2.21)
#
class DerivationMethodEnumeration(enum.IntEnum):
    DERIVE_PBKDF2        = 0x0001 
    DERIVE_HASH          = 0x0002 
    DERIVE_HMAC          = 0x0003 
    DERIVE_ENCRYPT       = 0x0004 
    DERIVE_NIST800108C   = 0x0005 
    DERIVE_NIST800108F   = 0x0006 
    DERIVE_NIST800108DPI = 0x0007 
    DERIVE_ASYMMETRICKEY = 0x0008 
	
	
# Alternative Name Type Enumeration (KMIP Soec section 9.1.3.2.34)
#
class AlternativeNameTypeEnumeration(enum.IntEnum): 
    ALTNAME_TEXTSTRING    = 0x0001         # Uninterpreted Text String
    ALTNAME_URI           = 0x0002 
    ALTNAME_SERIAL_NUMBER = 0x0003         # Object Serial Number
    ALTNAME_EMAILADDRESS  = 0x0004 
    ALTNAME_DNSNAME       = 0x0005 
    ALTNAME_X500DN        = 0x0006         # X.500 Distinguished Name
    ALTNAME_IPADDRESS     = 0x0007 


# Key Value Location Type (KVLT) Enumeration (KMIP Spec Section 9.1.3.2.35)
#
class KeyValueLocationTypeEnumeration(enum.IntEnum): 
    KVLT_TEXTSTRING = 0x0001      # Uninterpreted Text String
    KVLT_URI        = 0x0002
	
	
# Mask Generator Enumeration (KMIP Soec section 9.1.3.2.48)
#
class MaskGeneratorEnumeration(enum.IntEnum): 
    MASKGENERATOR_MGF1 = 0x0001

	
# Cryptographic Usage Mask (bit mask) (KMIP Spec Section 9.1.3.3.1)
#
# This s a bit mask that defines how the key is to be used
# Here we defined constants for the most commonly used values, see KMIP specification for others.
#
class CryptographicUsageMask(enum.IntEnum):
    USAGE_MASK_SIGN      = 0x000001
    USAGE_MASK_VERIFY    = 0x000002
    USAGE_MASK_ENCRYPT   = 0x000004
    USAGE_MASK_DECRYPT   = 0x000008
    USAGE_MASK_WRAPKEY   = 0x000010
    USAGE_MASK_UNWRAPKEY = 0x000020
    USAGE_MASK_MACGEN    = 0x000080
    USAGE_MASK_MACVERIFY = 0x000100
    USAGE_MASK_DERIVEKEY = 0x000200

	
