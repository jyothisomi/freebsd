#!/usr/bin/env python3

#---------Funtion API's used by fcdms application

import json
import sys
import os
from ast import literal_eval
import subprocess

sys.path.insert(0, '../seagate-pysed-python3/build/lib.linux-x86_64-3.6')
from sed_enablement.tcgapi import Sed,fcdmsSed


#*****************************************************************
#---Inf: Get the Drive data and update in Database
#---Par: None
#---Ret: Drive data converted to json
def getDriveData():
    #y = driveScan()
    parsedDriveData = [{"UUID" : "45b21943-8323", "SN":"2345", "WWN":"5000C5009509C1D7","DevicePath":"/dev/sdb", "TCGSupport":"2.0","LockedStatus":"False","FIPSSupport":"True","FIPSMode":"Disabled","BootDrive":"True","LockingRanges":"0","Managed":"True"},{"UUID" : "4879-874c", "SN":"ZAD3E4ER000", "WWN":"5000C5","DevicePath":"/dev/sdc", "TCGSupport":"Ent","LockedStatus":"False","FIPSSupport":"True","FIPSMode":"Enabled","BootDrive":"False","LockingRanges":"0,1,2","Managed":"False"},{"UUID" : "6c1cc6ccf5", "SN":"ZAD3E4ER000C","WWN":"5000C50095", "DevicePath":"/dev/sdd", "TCGSupport":"Ent","LockedStatus":"False","FIPSSupport":"True","FIPSMode":"Enabled","BootDrive":"No","LockingRanges":"0,1,2","Managed":"True"}]
    driveData = json.dumps(parsedDriveData)
    ret = updateDatabaseDriveDataTable(parsedDriveData)
    if ret is True:
        resp= {"STATUS" : "Success", "ERROR": "None", "RESULT": "Updated Data Drive Database Table"}
        return json.dumps(resp)
    else:
        resp= {"STATUS" : "Failure", "ERROR": "Cannot able to update Data Drive Table in Database", "RESULT": ""}
        return json.dumps(resp)


#********************************************************************
#---Inf: Get the Drive Pin data and update in Database
#---Par: None
#---Ret: Pin data converted to json
def getDrivePinData():
    parsedDrivePinData = [{"SN":"ZAD2345", "Authority":"BandMaster1","Enable_State":"RO","PIN_Source":"KMIP", "PIN_UUID":"5fddd5d6-2a23","PIN_Version":1},{"SN":"ZAD3E4ER000C82", "Authority":"BandMaster1","Enable_State":"RO","PIN_Source":"KMIP","PIN_UUID":"b029-ec40c1218dd7","PIN_Version":2},{"SN":"ZAD3E4ER000C8267", "Authority":"BandMaster0","Enable_State":"RW","PIN_Source":"DB","PIN_UUID":"b029-ec40c1218dd7","PIN_Version":2}]
    drivePinData = json.dumps(parsedDrivePinData)
    ret = updateDatabaseDrivePinTable(parsedDrivePinData)
    if ret is True:
        resp= {"STATUS" : "Success", "ERROR": "None", "RESULT": "Updated Data Drive Pin Database Table"}
        return json.dumps(resp)
    else:
        resp= {"STATUS" : "Failure", "ERROR": "Cannot able to update Data Drive Pin Table in Database", "RESULT": ""}
        return json.dumps(resp)


#********************************************************************

#---Inf: Get the Drive Scan data
#---Par: None
#---Ret: True/False and Server Verify data
def getDriveScan():
    try:
            getPysed = fcdmsSed()
            driveData=getPysed.scan()
            driveData = literal_eval("'%s'" % driveData).replace('\\','')
            getData = eval(driveData)
            for data in getData:
               drive = data["Device"]
               getSed = Sed(drive)
               tcgData = getSed.getTcgSSC()
               fipsData = getSed.getFIPSstate()
               if fipsData is None:
                   data["FiPS_Avail"] = "Not Supported"
               else:
                   data["Fips_Avail"] = "Yes"
               #print(fipsData)
               if "unknown" in tcgData.lower():
                   data["TCG"]="Not Supported"
               else:
                   data["TCG"] = tcgData
               
            #driveData = driveData.replace('\\','')
            #driveData = '[{"Vendor": "Seagate", "Device": "/dev/sda", "Model_Number":"ST1800MM0048", "TCG":"Ent", "FIPS_Avail":"Yes"},{"Vendor": "Seagate", "Device": "/dev/sdb", "Model_Number":"ST1800MM0048", "TCG":"Ent", "FIPS_Avail":"Yes"},{"Vendor": "Seagate", "Device": "/dev/sdc", "Model_Number": "ST1800MM0048", "TCG":"Ent", "FIPS_Avail":"Yes"},{"Vendor": "Seagate", "Device": "/dev/sdd", "Model_Number":"ST1800MM0048", "TCG":"Ent,Ent,Ent,Ent", "FIPS_Avail":"Yes"}]'
            return True,getData
    except:
        return False,None
#************************************************************************

#---Inf: Get the Scan Drive data
#---Par: None
#---ret: status of command execution and result in json
def scanDrives():
    #y = [{"vendor" : "seagate", "model":"SG234327687", "device":"/dev/sda", "TCG":"Ent", "FIPS":"Yes"},
    #     {"vendor" : "seagate", "model":"SG23432762332", "device":"/dev/sdb", "TCG":"Opal", "FIPS":"Yes"}
    #    ]
    #return json.dumps(y)
    status = ""
    error = ""
    result = ""

    ret,resp = getDriveScan()

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in getting Drive Scan Data"
        result= ''

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    return literal_eval("'%s'" % json.dumps(resp)).replace('\\','')


#************************************************************************

#---Inf: Get the Drive Query Data
#---Par: user arguments from panansas application
#---Ret: True/False and Server Verify data
def getDriveQuery(arguments):
    try:
        drive = None
        drive = arguments["drive"]
        if drive:
            getPysed = fcdmsSed()
            driveData=getPysed.driveQuery(drive)
            #print(driveData)
            #driveData1 = {"drive":drive, "data":"Drive Query Data"}
            #print(driveData)
            #resp = subprocess.getoutput("../openseaChest_Binaries/SeaChest_Security -d "+drive+" -i")
            #driveData = resp.split("\n",7)[7];
            #return True,driveData1
            return True,eval(driveData)
        else:
            return False,"Error in getting Drive Information"
    except:
        return False,None
#************************************************************************
#---Inf: TCG Drive Query
#---Par: user arguments from panansas application
#---Ret: Status of command execution and result in json
def driveQuery(arguments):
    status = ""
    error = ""
    result = ""

    ret,resp = getDriveQuery(arguments)

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in Getting Query Data"
        result= resp

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}
    #print(result)    
    #return json.dumps(resp)
    return literal_eval("'%s'" % json.dumps(resp)).replace('\\','')
    #return(result)
#************************************************************************

#---Inf: Get the Tcg Information Data
#---Par: user arguments from panansas application
#---Ret: True/False and tcg data
def getTcgInfo(arguments):
        drive = None
        drive = arguments["drive"]
    #try:
        if drive:
            #Process to generate CSR
            #Data = "Server Query Data"
                resp = subprocess.getoutput("../openseaChest_Binaries/SeaChest_Security -d "+drive+" --tcgInfo")
                driveData = resp.split("\n",7)[7];
            #getTCG = Sed(drive)
            #driveData=getTCG.getTcgSSC()
            #if "unknown" in driveData.lower():
                #return False, "The Drive won't support TCG Features"
            #else:
                #driveData = {"drive":drive, "data":"TCG Drive Information"}
                return True,driveData
        else:
            return False,"No drive Data"
    #except:
        #return False,"Error in getting Tcg Information"
#************************************************************************

#---Inf: TCG Information
#---Par: user arguments from panansas application
#---Ret: Status of command execution and result in json
def tcgInfo(arguments):
    status = ""
    error = ""
    result = ""

    ret,resp = getTcgInfo(arguments)

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in Getting TCG Information Data"
        result= resp

    #resp= {"STATUS" : status, "ERROR": error, "RESULT": result}
    return result
    #return json.dumps(resp)
#************************************************************************
#---Inf: Get the Drive Status
#---Par: None
#---Ret: True/False and Drive Status data
def getDriveStatus():
    #try:
            #Process to generate CSR
            #Data = "Server Query Data"
            ret,driveData = getDriveScan()
            #Remove Vendor details
            #Add the column Data Protection and Managed State
            for data in driveData:
                del data["Vendor"]
                data["DataProtection"] = "Not Implemented"
                data["ManagedState"] = "Not Implemented"
            
            
            #driveData = [{"Device":"/dev/sda", "Model Number":"ST1800MM0048", "TCG Version": "Ent","FIPS Capable": "Yes", "Data Protection": "Enabled", "Managed State": "Yes"},{"Device":"/dev/sdb", "Model Number": "ST1800MM0048", "TCG Version": "Ent","FIPS Capable": "Yes", "Data Protection": "Enabled", "Managed State": "Yes"},{"Device":"/dev/sdc", "Model Number": "ST1800MM0048", "TCG Version": "Ent","FIPS Capable": "Yes", "Data Protection": "Enabled", "Managed State": "Yes"},{"Device":"/dev/sdd", "Model Number": "ST1800MM0048", "TCG Version": "Ent","FIPS Capable": "Yes", "Data Protection": "Enabled", "Managed State": "Yes"},{"Device":"/dev/sde", "Model Number":"ST1800MM0048", "TCG Version": "Ent","FIPS Capable": "Yes", "Data Protection": "Enabled", "Managed State": "Yes"},{"Device":"/dev/sdf", "Model Number":"ST1800MM0048", "TCG Version": "Ent","FIPS Capable": "Yes", "Data Protection": "Enabled", "Managed State": "Yes"}]
            return True,driveData
    #except:
        #return False,None
#************************************************************************

#---Inf: TCG Drive Status 
#---Par: None
#---Ret: Status of command execution and result in json
def driveStatus():
    status = ""
    error = ""
    result = ""

    ret,resp = getDriveStatus()

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in Getting Drive Status"
        result= ''

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    #return json.dumps(resp)
    return literal_eval("'%s'" % json.dumps(resp)).replace('\\','')
#************************************************************************



