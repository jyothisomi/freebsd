#!/usr/bin/env python3

#---------Funtion API's used by fcdms application

import json
import sys
import os


sys.path.insert(0, '../seagate-pysed-python3/build/lib.linux-x86_64-3.6')
from sed_enablement.tcgapi import Sed,fcdmsSed

#************************************************************************
#---Inf: Erase the Drive
#---Par: drive
#---Ret: True/False and status of erase
def getEraseDrive(arguments):
    drive = None
    drive = arguments["drive"]

    try:
      if drive:
         serData = {"drive":drive,"status": "reset"}
         return True,serData
      else:
         return False,None

    except:
        return False,None
#************************************************************************

#---Inf: Erase the Drive
#---Par: user arguments from panansas application
#---Ret: Status of command execution and result in json
def eraseDrive(arguments):
    status = ""
    error = ""
    result = ""

    ret,resp = getEraseDrive(arguments)

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in Erasing Drive "
        result= ''

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    return json.dumps(resp)
#************************************************************************
#---Inf: deactivate locking range
#---Par: range and drive
#---Ret: True/False and status of disable
def getDisableAuthority(arguments):
    bandType = None
    drive = None
    bandType = arguments["range"]
    drive = arguments["drive"]

    try:
      if drive:
         serData = {"drive":drive,"range": bandType, "status": "removed"}
         return True,serData
      else:
         return False,None

    except:
        return False,None
#************************************************************************

#---Inf: Disable the locking range  
#---Par: user arguments from panansas application
#---Ret: Status of command execution and result in json
def disableAuthority(arguments):
    status = ""
    error = ""
    result = ""

    ret,resp = getDisableAuthority(arguments)

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in disabling authority "
        result= ''

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    return json.dumps(resp)
#************************************************************************
#---Inf: Erase the drive using PSID
#---Par: psid and drive
#---Ret: True/False and status of erase
def getRevertPSID(arguments):
    psid = None
    drive = None
    psid = arguments["psid"]
    drive = arguments["drive"]

    try:
      if drive:
         serData = {"drive":drive,"status": "reset"}
         return True,serData
      else:
         return False,None

    except:
        return False,None
#************************************************************************

#---Inf: Revert the Drive using PSID
#---Par: user arguments from panansas application
#---Ret: Status of command execution and result in json
def revertPSID(arguments):
    status = ""
    error = ""
    result = ""

    ret,resp = getRevertPSID(arguments)

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in reverting Drive using PSID "
        result= ''

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    return json.dumps(resp)
#************************************************************************




