#!/usr/bin/env python
#
# Module to open a YAML file used for configuration info
#

# Libraries/Modules
import os
import yaml

# TODO ! add logging

# TODO! Ensure default path for fc-cmp set in config.yaml
# TODO! Add better strong typing - str, int, bool, obj, List, Dict, Tupl, etc

class YAMLConfig():
    # open and parse a YAML file
    @staticmethod
    def getYAMLConfig():
        try:
            #Parse YAML file and return object generated
            with open(os.path.abspath(os.path.join(os.path.dirname(__file__),
                                                   'configs/config.yaml')), 'r') as ymlfile:

                # Note we are using cross platform SAFE yaml.load by specifying Loader=yaml.Fullloader
                cfg = yaml.load(ymlfile, Loader=yaml.FullLoader)
            return cfg

        except Exception as e:
            print(e)
