#!/usr/bin/env python3

#---------Funtion API's used by fcdms application

import json
import sys
import os


sys.path.insert(0, '../seagate-pysed-python3/build/lib.linux-x86_64-3.6')
from sed_enablement.tcgapi import Sed,fcdmsSed

#************************************************************************
#---Inf: Get the activation of locking ranges 
#---Par: user data details and drive
#---Ret: True/False and locking ranges data
def getActivateLockingRanges(arguments):
    try:
            #Process to generate CSR
            #Data = "Server Query Data"

            rangeData = [{"ActiveKey": "0x0000080600000002", "commonName":"Locking", "LockOnReset":"False", "Name":"Band1", "RangeLength":219749770,"RangeStart":4097,"ReadLockEnabled":1,"ReadLocked":0,"UID":"0x0000080200000002", "WriteLockEnabled":0, "_AllowATAUnlock":0},{"ActiveKey": "0x0000080600000003", "commonName":"Locking", "LockOnReset":"False", "Name":"Band2", "RangeLength":219747779,"RangeStart":219791218,"ReadLockEnabled":1,"ReadLocked":0,"UID":"0x0000080200000003", "WriteLockEnabled":0, "_AllowATAUnlock":0}]
            return True,rangeData
    except:
        return False,None
#************************************************************************

#---Inf: Activate Locking Ranges with the user date
#---Par: user arguments from panansas application
#---Ret: Status of command execution and result in json
def activateLockingRanges(arguments):
    status = ""
    error = ""
    result = ""

    ret,resp = getActivateLockingRanges(arguments)

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in activating Locking ranges "
        result= ''

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    return json.dumps(resp)
#************************************************************************
#---Inf: Get the activation of locking range for only one band
#---Par: user data details and drive
#---Ret: True/False and locking range data
def getActivateLockingRange(arguments):
    try:
            #Process to generate CSR
            #Data = "Server Query Data"

            rangeData = {"ActiveKey": "0x0000080600000002", "commonName":"Locking", "LockOnReset":"False", "Name":"Band1", "RangeLength":219749770,"RangeStart":4097,"ReadLockEnabled":1,"ReadLocked":0,"UID":"0x0000080200000002", "WriteLockEnabled":0, "_AllowATAUnlock":0}
            return True,rangeData
    except:
        return False,None
#************************************************************************

#---Inf: Activate Locking Range with the user date
#---Par: user arguments from panansas application
#---Ret: Status of command execution and result in json
def activateLockingRange(arguments):
    status = ""
    error = ""
    result = ""

    ret,resp = getActivateLockingRange(arguments)

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in activating Locking range "
        result= ''

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    return json.dumps(resp)
#************************************************************************
#---Inf: set the Locking Range
#---Par: Table type and drive
#---Ret: True/False and table data 
def getLockingRangeStatus(arguments):
    status = None
    drive = None
    status = arguments["status"]
    drive = arguments["drive"]

    try:
      if drive:
         if "enable" in status:
            #Process to generate CSR
            #Data = "Server Query Data"

            serData = {"Name": "Band1", "Range Start": 4097, "Range Length": 219749770,"Read Status": "Enabled", "Write Status": "Enabled", "Read Locked":"False", "Write Locked": "False", "Lock on Reset": "True"}
            return True,serData
         elif "disable" in status:
            serData = {"Name": "Band1", "Range Start": 4097, "Range Length": 219749770,"Read Status": "Disabled", "Write Status": "Disabled", "Read Locked":"False", "Write Locked": "False", "Lock on Reset": "False"}
            return True,serData
         else:
            return False,None
    except:
        return False,None
#************************************************************************

#---Inf: Set the Locking range Status 
#---Par: user arguments from panansas application
#---Ret: Status of command execution and result in json
def setStatusLockingRange(arguments):
    status = ""
    error = ""
    result = ""

    ret,resp = getLockingRangeStatus(arguments)

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in setting the Locking range status "
        result= ''

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    return json.dumps(resp)
#************************************************************************
#---Inf: set the Locking Range to LK,RW,RO
#---Par: type, state and drive
#---Ret: True/False and range status after setting
def getSetRange(arguments):
    bandType = None
    state = None
    drive = None
    bandType = arguments["range"]
    state = arguments["state"]
    drive = arguments["drive"]

    try:
      if drive:
         if "LK" in state:
            #Process to generate CSR
            #Data = "Server Query Data"

            serData = {"Name": "Band1", "Range Start": 4097, "Range Length": 219749770,"Read Status": "Enabled", "Write Status": "Enabled", "Read Locked":"True", "Write Locked": "True", "Lock on Reset": "True"}
            return True,serData
         elif "RW" in state:
            serData = {"Name": "Band1", "Range Start": 4097, "Range Length": 219749770,"Read Status": "Disabled", "Write Status": "Disabled", "Read Locked":"False", "Write Locked": "False", "Lock on Reset": "True"}
            return True,serData
         elif "RO" in state:
            serData = {"Name": "Band1", "Range Start": 4097, "Range Length": 219749770,"Read Status": "Disabled", "Write Status": "Disabled", "Read Locked":"False", "Write Locked": "True", "Lock on Reset": "True"}
            return True,serData
         else:
            return False,None
    except:
        return False,None
#************************************************************************

#---Inf: Set the Locking range 
#---Par: user arguments from panansas application
#---Ret: Status of command execution and result in json
def setRange(arguments):
    status = ""
    error = ""
    result = ""

    ret,resp = getSetRange(arguments)

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in setting the Locking range state "
        result= ''

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    return json.dumps(resp)
#************************************************************************
#---Inf: configure drive to RW,RO
#---Par: type, state and drive
#---Ret: True/False and managed drive status after setting
def getManageDriveEnable(arguments):
    bandType = None
    state = None
    drive = None
    bandType = arguments["range"]
    state = arguments["state"]
    drive = arguments["drive"]

    try:
      if drive:
         if "RW" in state:
            serData = {"Drive":drive, "range":bandType,"state":"ReadWrite"}
            return True,serData
         elif "RO" in state:
            serData = {"Drive":drive, "range":bandType,"state":"ReadOnly"}
            return True,serData
         else:
            return False,None
    except:
        return False,None
#************************************************************************

#---Inf: Configure the Managed drive 
#---Par: user arguments from panansas application
#---Ret: Status of command execution and result in json
def manageDriveEnable(arguments):
    status = ""
    error = ""
    result = ""

    ret,resp = getManageDriveEnable(arguments)

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in managing the drive "
        result= ''

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    return json.dumps(resp)
#************************************************************************

#---Inf: Get the status of unlock all drives 
#---Par: None
#---Ret: True/False and unlock all drives data
def getUnlockRanges():
    try:

            rangeData = [{"Drive": "/dev/sda", "Band": "Band1,Band2", "Status":"RO.RW"}, {"Drive": "/dev/sdb", "Band": "Band1,Band2", "Status":"RO.RW"}, {"Drive": "/dev/sdc", "Band": "Band1,Band2", "Status":"RO.RW"}, {"Drive": "/dev/sdd", "Band": "Band1,Band2", "Status":"RO.RW"}, {"Drive": "/dev/sde", "Band": "Band1,Band2", "Status":"RO.RW"}]
            return True,rangeData
    except:
        return False,None
#************************************************************************

#---Inf: Unlock all managed Drives
#---Par: None
#---ret: status of command execution and result in json
def unlockRanges():
    status = ""
    error = ""
    result = ""

    ret,resp = getUnlockRanges()

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in unlocking managed drives"
        result= ''

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    return json.dumps(resp)


#************************************************************************
#---Inf: Rekey all locking PINS for all ranges and single range
#---Par: range and drive
#---Ret: True/False and status of changed locking pins
def getChangeLockingPINS(arguments):
    bandType = None
    drive = None
    bandType = arguments["range"]
    drive = arguments["drive"]

    try:
      if drive:
         if "all" in bandType and "all" in drive:
            serData = [{"drive": "/dev/sda", "range":"Band1", "status": "changed"},{"drive": "/dev/sda", "range": "Band2", "status": "changed"},{"drive": "/dev/sdb", "range":"Band1", "status": "changed"},{"drive": "/dev/sdb", "range":"Band2", "status": "changed"}]
            return True,serData
         elif "all" in bandType:
            serData = [{"drive": "/dev/sda", "range":"Band1", "status": "changed"},{"drive": "/dev/sda", "range": "Band2", "status": "changed"}]
            return True,serData
         else:
            serData = {"drive": drive, "range":bandType, "status": "changed"}
            return True,serData
            
    except:
        return False,None
#************************************************************************

#---Inf: Change the locking pins status 
#---Par: user arguments from panansas application
#---Ret: Status of command execution and result in json
def changeLockingPINS(arguments):
    status = ""
    error = ""
    result = ""

    ret,resp = getChangeLockingPINS(arguments)

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in rekeying the locking PINS "
        result= ''

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    return json.dumps(resp)
#************************************************************************
#---Inf: Get all the locking ranges using KMIP
#---Par: range and drive
#---Ret: True/False and status of data locking ranges
def getKMIPLockingRanges(arguments):
    bandType = None
    drive = None
    bandType = arguments["range"]
    drive = arguments["drive"]

    try:
      if drive:
         if "all" in bandType :
            serData = [{"Drive":"/dev/sda","range": "Band0", "Range Start": 0, "Range Length": 0,"Read Status": "Enabled", "Write Status": "Enabled", "Read Locked":"False", "Write Locked": "False", "Lock on Reset": "True"},{"Drive":"/dev/sda","range": "Band1", "Range Start": 4097, "Range Length": 219749770,"Read Status": "Enabled", "Write Status": "Enabled", "Read Locked":"False", "Write Locked": "False", "Lock on Reset": "True"},{"Drive":"/dev/sda","range": "Band2", "Range Start": 219791218, "Range Length": 219747779,"Read Status": "Enabled", "Write Status": "Enabled", "Read Locked":"False", "Write Locked": "False", "Lock on Reset": "True"},{"Drive":"/dev/sdb","range": "Band0", "Range Start": 0, "Range Length": 0,"Read Status": "Enabled", "Write Status": "Enabled", "Read Locked":"False", "Write Locked": "False", "Lock on Reset": "True"},{"Drive":"/dev/sdb","range": "Band1", "Range Start": 4097, "Range Length": 219749770,"Read Status": "Enabled", "Write Status": "Enabled", "Read Locked":"False", "Write Locked": "False", "Lock on Reset": "True"},{"Drive":"/dev/sdb","range": "Band2", "Range Start": 219791218, "Range Length": 219747779,"Read Status": "Enabled", "Write Status": "Enabled", "Read Locked":"False", "Write Locked": "False", "Lock on Reset": "True"}]
            return True,serData
         else:
            serData = {"drive": drive, "range":bandType, "Range Start": 4097, "Range Length": 219749770,"Read Status": "Enabled", "Write Status": "Enabled", "Read Locked":"False", "Write Locked": "False", "Lock on Reset": "True"}
            return True,serData

    except:
        return False,None
#************************************************************************

#---Inf: Get all locking Ranges
#---Par: user arguments from panansas application
#---Ret: Status of command execution and result in json
def getLockingRanges(arguments):
    status = ""
    error = ""
    result = ""

    ret,resp = getKMIPLockingRanges(arguments)

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in getting Locking ranges "
        result= ''

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    return json.dumps(resp)
#************************************************************************
#---Inf: Erase the locking range
#---Par: range and drive
#---Ret: True/False and status of erase
def getEraseRange(arguments):
    bandType = None
    drive = None
    bandType = arguments["range"]
    drive = arguments["drive"]

    try:
      if drive:
         serData = {"drive":drive,"range": bandType, "status": "erased"}
         return True,serData
      else:
         return False,None

    except:
        return False,None
#************************************************************************

#---Inf: Erase the locking range  
#---Par: user arguments from panansas application
#---Ret: Status of command execution and result in json
def eraseRange(arguments):
    status = ""
    error = ""
    result = ""

    ret,resp = getEraseRange(arguments)

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in Erasing Locking ranges "
        result= ''

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    return json.dumps(resp)

