#!/usr/bin/env python3

#---------Funtion API's used by fcdms application

import json
import sys
import os


sys.path.insert(0, '../seagate-pysed-python3/build/lib.linux-x86_64-3.6')
from sed_enablement.tcgapi import Sed,fcdmsSed

#************************************************************************

#---Inf: configure device
#---Par: user arguments from panansas application
#---Ret: True/False and configured device status
def getDeviceConfigure(arguments):
    drive = None
    status = None
    drive = arguments["drive"]
    status = arguments["status"]

    try:
      if drive:
         if "unmanage" in status:
            #Process to generate CSR
            #Data = "Server Query Data"

            serData = {"drive":drive,"status": "removed"}
            return True,serData
         elif "manage" in status:
            serData = {"drive":drive,"status": "added"}
            return True,serData
         else:
            return False,None
    except:
        return False,None
#************************************************************************
#---Inf: configure Device for managed or un managed
#---Par: user arguments from panansas application
#---Ret: Status of command execution and result in json
def deviceConfigure(arguments):
    status = ""
    error = ""
    result = ""

    ret,resp = getDeviceConfigure(arguments)

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in Getting Device configuration for managed or unmanaged"
        result= ''

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    return json.dumps(resp)

#************************************************************************

#---Inf: get Drive ownership
#---Par: user arguments from panansas application
#---Ret: True/False and ownership drive data response
def getDriveOwnership(arguments):
    drive = None
    method = None
    drive = arguments["drive"]
    method = arguments["method"]

    try:
      if drive:
         if "kmip" in method:
            #Process to generate CSR
            #Data = "Server Query Data"

            serData = {"drive":drive,"method": method,"status":"registered"}
            return True,serData
         elif "msid" in method:
            serData = {"drive":drive,"method": method,"status":"registered"}
            return True,serData
         elif "aes" in method:
            serData = {"drive":drive,"method": method,"status":"registered"}
            return True,serData
         else:
            return False,None
    except:
        return False,None
#************************************************************************

#---Inf: Drive ownership with kmip or misid of aes
#---Par: user arguments from panansas application
#---Ret: Status of command execution and result in json
def driveOwnership(arguments):
    status = ""
    error = ""
    result = ""

    ret,resp = getDriveOwnership(arguments)

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in Getting response from drive Ownership"
        result= ''

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    return json.dumps(resp)
#************************************************************************

#---Inf: get status of authority change pin
#---Par: user arguments from panansas application
#---Ret: True/False and tcg change pin response
def getChangeTcgPin(arguments):
    drive = None
    authority = None
    attribute = None
    drive = arguments["drive"]
    authority = arguments["authority"]
    attribute = arguments["attribute"]

    try:
      if drive:
         if "None" in attribute:
            #Process to generate CSR
            #Data = "Server Query Data"

            serData = {"drive":drive,"authority": authority,"PINChanged":"True"}
            return True,serData
         else:
            serData = {"drive":drive,"authority": authority,"PINChanged":"True","CustomAttributes":"OK"}
            return True,serData
    except:
        return False,None
#************************************************************************

#---Inf:change TCG authority Pin
#---Par: user arguments from panansas application
#---Ret: Status of command execution and result in json
def changeTcgPin(arguments):
    status = ""
    error = ""
    result = ""

    ret,resp = getChangeTcgPin(arguments)

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in Getting response from change tcg authority pin"
        result= ''

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    return json.dumps(resp)
#************************************************************************

#---Inf: get the fips mode status and change the mode 
#---Par: user arguments from panansas application
#---Ret: True/False and fips mode response
def getFipsMode(arguments):
    method = None
    drive = None
    method = arguments["method"]
    drive = arguments["drive"]
    
    try:
      if drive:
         if "status" in method:
            #Process to generate CSR
            #Data = "Server Query Data"

            serData = {"support":"FIPS 140-2 Level 2","mode":"disabled"}
            return True,serData
         elif "enable" in method:
            serData = {"check_status": "pass", "disable_authority": "complete", "set_port_lock": "complete", "set_port_lockOnReset":"complete", "verify_mode_enable":"pass"}
            return True,serData
         elif "disable" in method:
            serData = {"enable_authority": "complete", "set_port_unlock": "complete", "set_port_unlockOnReset":"complete", "verify_mode_disable":"pass"}
            return True,serData
         else:
            return False,None
    except:
        return False,None
#************************************************************************

#---Inf:check the FIPS mode status , enable or disbale
#---Par: user arguments from panansas application
#---Ret: Status of command execution and result in json
def fipsMode(arguments):
    status = ""
    error = ""
    result = ""

    ret,resp = getFipsMode(arguments)

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in Getting response from Fips Mode"
        result= ''

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    return json.dumps(resp)
#************************************************************************
#---Inf: Get the port Settings
#---Par: drive details
#---Ret: True/False and port settings
def getPortSetting(arguments):
    try:
            #Process to generate CSR
            #Data = "Server Query Data"

            portData = {"download": "unlock", "port_lockOnReset":"unlock", "UDS":"lock"}
            return True,portData
    except:
        return False,None
#************************************************************************
#---Inf:check the Port settings of firmware download
#---Par: user arguments from panansas application
#---Ret: Status of command execution and result in json
def getPort(arguments):
    status = ""
    error = ""
    result = ""

    ret,resp = getPortSetting(arguments)

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in Getting response from port setting"
        result= ''

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    return json.dumps(resp)
#************************************************************************

#---Inf: get the firmware port setting response
#---Par: user arguments from panansas application
#---Ret: True/False and port setting
def getFirmwarePort(arguments):
    method = None
    drive = None
    method = arguments["method"]
    drive = arguments["drive"]

    try:
      if drive:
         if "unlock" in method:
            #Process to generate CSR
            #Data = "Server Query Data"

            serData = {"download": "unlock", "port_lockOnReset":"unlock"}
            return True,serData
         elif "lock" in method:
            serData = {"download": "lock", "port_lockOnReset":"lock"}
            return True,serData
         else:
            return False,None
    except:
        return False,None
#************************************************************************
#---Inf: Set the Firmware Port
#---Par: user arguments from panansas application
#---Ret: Status of command execution and result in json
def setFirmwarePort(arguments):
    status = ""
    error = ""
    result = ""

    ret,resp = getFirmwarePort(arguments)

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in setting the firmware port "
        result= ''

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    return json.dumps(resp)
#************************************************************************

