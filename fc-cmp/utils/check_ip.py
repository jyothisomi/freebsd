#!/usr/bin/env python
#
# Check FQDN and IP address and ensure that IP4 address is 4 octets
# Ensure that IP address octets fall between 0-255 and that FQDN is
# not greater than 253 characters and less than 0 characters. Also
# check that FQDN conforms to special character requirements via regex
#

# Libraries/Modules
import ipaddress
from socket import getaddrinfo, gethostbyname
import re


# TODO ! add logging
# TODO! Add better strong typing - str, int, bool, obj, List, Dict, Tupl, etc

class CheckFQDN():

    # validate an FQDN
    def validateFQDN(self, address):
        try:
            self.isIP = CheckFQDN.test_IP(address)

            if self.isIP:
               self.result = CheckFQDN.validateIP(address)
               return self.result
            else:
                self.result = CheckFQDN.test_FQDN(self, address)

            if self.result == False:
                return False

            else:
                self.ip = getaddrinfo(address, None)
                self.addr = self.ip[4][0]
                self.isFQDN = CheckFQDN.test_IP(self.addr)
                if self.isFQDN:
                    return True
                else:
                    return False
        except:
            print ("Not valid FQDN")


    # validate an IP add4ess
    def validateIP(self, address):

        # Make a regular expression for validating an Ip-address
        regex = '''^(25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.( 
                    25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.( 
                    25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.( 
                    25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)'''

        try:
            # pass the regular expression and the string in search() method
            if (re.search(regex, address)):
                ip = ipaddress.ip_address(address)
                return True
            else:
                return False
        except ValueError:
            print('address/netmask is invalid: %s' % address)
            return False
        except:
            print('Usage: %s ip' % address)

    # test the characters of the IP address for validity
    def test_IP(self, address):
        addr = address.split('.')
        if len(addr) != 4:
            return False
        for x in addr:
            if not x.isdigit():
                return False
            i = int(x)
            if i < 0 or i > 255:
                return False
        return True

    # test the characters of the FQDN address for validity
    def test_FQDN(self, dn):
         if dn.endswith('.'):
             dn = dn[:-1]
         if len(dn) < 1 or len(dn) > 253:
             return False
         ldh_re = re.compile('^[a-z0-9]([a-z0-9-]{0,61}[a-z0-9])?$', re.IGNORECASE)
         return all(ldh_re.match(x) for x in dn.split('.'))
