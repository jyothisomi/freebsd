#!/usr/bin/env python3

#---------Funtion API's used by fcdms application

import json
import sys
import os

from pysqlcipher3 import dbapi2 as sqlcipher
from ast import literal_eval

sys.path.insert(0, '../kmip')

from csr_request import X509Certificate 
DB_PATH = '/fccmpDB/fccmpEncrypt.db'
CSRFILE = '/CNCSR'
SERVER_PORT = 5696
SERVER_RECONNECT = 0
SERVER_NBIO = 0
SERVER_TIMEOUT = 6

ServerCfgData=[]

if not os.path.exists('/fccmpDB'):
    os.system('mkdir /fccmpDB')

#*****************************************************************

def createDatabaseConfigTable(kmipData):
    try:
        sql_create_config_table = '''CREATE TABLE IF NOT EXISTS `configDB` ( 
                                    `SrNo` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                                    `KMIP_Enabled` TEXT NOT NULL,
                                    `KMIP_Batch_Mode` TEXT NOT NULL,
                                    `KMIP_Client_CSR` BLOB NOT NULL,
                                    `KMIP_Client_Private_Key` BLOB NOT NULL,
                                    `KMIP_Client_Public_Key` BLOB NOT NULL,
                                    `KMIP_Server_Name` TEXT NOT NULL,
                                    `KMIP_Server_Host_Address` TEXT NOT NULL,
                                    `KMIP_Server_CA_Cert` BLOB NOT NULL,
                                    `KMIP_Version` TEXT NOT NULL);'''

        # Create a Database connection
        conn = create_connection(DB_PATH)
        #create table if not exist
        if conn is not None:
            ret = create_table(conn, sql_create_config_table)
        else:
            print("Error! cannot create the database connection.")
        if ret is True:
          for data in kmipData:
            conn.execute('pragma key="testPassword"')
            conn.execute("INSERT INTO configDB (`KMIP_Enabled`, `KMIP_Batch_Mode`, `KMIP_Client_CSR`, `KMIP_Client_Private_Key`, `KMIP_Client_Public_Key`, `KMIP_Server_Name`,`KMIP_Server_Host_Address`,`KMIP_Server_CA_Cert`,`KMIP_Version`) VALUES (?,?,?,?,?,?,?,?,?)", (data["KMIP_Enabled"], data["KMIP_Batch_Mode"], data["KMIP_Client_CSR"],data["KMIP_Client_Private_Key"],data["KMIP_Client_Public_Key"], data["KMIP_Server_Name"], data["KMIP_Server_Host_Address"], data["KMIP_Server_CA_Cert"], data["KMIP_Version"]))
          conn.commit()
          conn.close()
          return True
        else:
           return False
    except:
        #print("Error in connection with database")
        return False
#*****************************************************************

def updateDatabaseConfigTable(kmipData):
    try:
        if "kmip_servername" in kmipData:
            serverName = kmipData["kmip_servername"]
        else:
            serverName = None
        nameChk = 0
        if serverName:
            # Create a Database connection
            conn = create_connection(DB_PATH)
            if conn is not None:
                conn.execute('pragma key="testPassword"')
                #for data in newKmipData:
                  #print(data)  
                for (k,v) in kmipData.items():
                    col = None
                    if "kmip_servername" not in k:
                        if "KMIPprotocol" in k:
                            col = "KMIP_Version"
                        elif "kmip_address" in k:
                            col = "KMIP_Server_Host_Address"
                        elif "client_key" in k:
                            col = "KMIP_Client_Private_Key"
                        elif "client_cert" in k:
                            col = "KMIP_Client_Public_Key"
                        elif "KMIPCA_cert" in k:
                            col = "KMIP_Server_CA_Cert"
                        else:
                            col = None
                        if col is not None:
                            conn.execute("UPDATE configDB SET (" + col + ")=? where `KMIP_Server_Name`=?",(kmipData[k],serverName))
                conn.commit()
                conn.close()
                return True
            else:
                print("Error in connection with database")
                return False
        else:
           return False
    except:
        #print("Error in connection with database")
        return False

#*****************************************************************

def delDatabaseConfigTable(kmipData):
    try:
        if "kmip_servername" in kmipData:
            serverName = kmipData["kmip_servername"]
        else:
            serverName = None
        nameChk = 0
        if serverName:
            # Create a Database connection
            conn = create_connection(DB_PATH)
            if conn is not None:
                conn.execute('pragma key="testPassword"')
                conn.execute("DELETE FROM configDB where `KMIP_Server_Name`=?",(serverName,))
                conn.commit()
                conn.close()
                return True
            else:
                #print("Error in connection with database")
                return False
        else:
           return False
    except:
        #print("Error in connection with database")
        return False

#*****************************************************************

def updateDatabaseDrivePinTable(driveData):
    try:
        sql_create_drivePin_table = '''CREATE TABLE IF NOT EXISTS `drivePin` ( 
                                    `DrivePinNo` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                                    `SN` TEXT NOT NULL,
                                    `Authority` TEXT NOT NULL,
                                    `Enable_State` TEXT NOT NULL,
                                    `PIN_Source` TEXT NOT NULL,
                                    `PIN_UUID` TEXT NOT NULL,
                                    `PIN_Version` INTEGER NOT NULL);'''

        # Create a Database connection
        conn = create_connection(DB_PATH)
        
        #create table if not exist
        if conn is not None:
            ret = create_table(conn, sql_create_drivePin_table)
        else:
            print("Error! cannot create the database connection.")
                
        if ret is True:
          for data in driveData:
            conn.execute('pragma key="testPassword"')  
            conn.execute("INSERT INTO drivePin (`SN`, `Authority`, `Enable_State`, `PIN_Source`, `PIN_UUID`, `PIN_Version`) VALUES (?,?,?,?,?,?)", (data["SN"], data["Authority"], data["Enable_State"], data["PIN_Source"], data["PIN_UUID"], data["PIN_Version"]))
          conn.commit()
          conn.close()
          return True
        else:
          return False
    except:
        print("Error in connection with database")
        return False

#*****************************************************************
def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlcipher.connect(db_file)
        conn.execute('pragma key="testPassword"')
        return conn
    except :
        print("Error in creating Connection with the Databse")

    return conn
#*****************************************************************
def create_table(conn, create_table_sql):
    """ create a table from the create_table_sql statement
    :param conn: Connection object
    :param create_table_sql: a CREATE TABLE statement
    :return:
    """
    try:
        conn.execute('pragma key="testPassword"')
        conn.execute(create_table_sql)
        return True
    except :
        return False

#*****************************************************************
def updateDatabaseDriveDataTable(driveData):
    try:
        sql_create_driveData_table = '''CREATE TABLE IF NOT EXISTS `driveData` ( 
                                    `DriveNo` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                                    `UUID` TEXT NOT NULL,
                                    `SN` TEXT NOT NULL,
                                    `WWN` TEXT NOT NULL,
                                    `Device Path` TEXT NOT NULL,
                                    `TCG Support` TEXT NOT NULL,
                                    `Locked Status` TEXT NOT NULL,
                                    `FIPS Support` TEXT NOT NULL,
                                    `FIPS Mode` TEXT NOT NULL,
                                    `Boot Drive` TEXT NOT NULL,
                                    `Locking Ranges` TEXT NOT NULL,
                                    `Managed` TEXT NOT NULL );'''

        # Create a Database connection
        conn = create_connection(DB_PATH)
        
        #create table if not exist
        if conn is not None:
            ret = create_table(conn, sql_create_driveData_table)
        else:
            print("Error! cannot create the database connection.")
                
        if ret is True:
          conn.execute('pragma key="testPassword"')
          for data in driveData:
            conn.execute("INSERT INTO driveData (`UUID`, `SN`, `WWN`, `Device Path`, `TCG Support`, `Locked Status`, `FIPS Support`, `FIPS Mode`, `Boot Drive`, `Locking Ranges`, `Managed`) VALUES (?,?,?,?,?,?,?,?,?,?,?)", (data["UUID"], data["SN"], data["WWN"], data["DevicePath"], data["TCGSupport"], data["LockedStatus"], data["FIPSSupport"], data["FIPSMode"], data["BootDrive"], data["LockingRanges"], data["Managed"]))
          conn.commit()
          conn.close()
          return True
        else:
          return False
    except:
        #print("Error in connection with database")
        return False

#************************************************************************

#---Inf:save the client certificate files
#---Par: user arguments from panansas application 
#---Ret: success: True  ,  Failure : False
def saveServerCertFiles(arguments):
    try:
        #if not os.path.exists(os.environ['HOME']+'/clientCert'):
        if not os.path.exists('/clientCert'):
            #os.system('mkdir '+os.environ['HOME']+'/clientCert')
            os.system('mkdir /clientCert')
            #os.system('sudo chmod -R 777 /clientCert')
        #if not os.path.exists(os.environ['HOME']+'/clientKey'):
        if not os.path.exists('/clientKey'):
            #os.system('mkdir '+os.environ['HOME']+'/clientKey')
            os.system('mkdir /clientKey')
            #os.system('sudo chmod -R 777 /clientKey')
        #if not os.path.exists(os.environ['HOME']+'/serverCA'):
        if not os.path.exists('/serverCA'):
            #os.system('mkdir '+os.environ['HOME']+'/serverCA')
            os.system('mkdir /serverCA')
            #os.system('sudo chmod -R 777 /serverCA')

        serverName = arguments["kmip_servername"]
        #certFilePath = os.environ['HOME']+"/clientCert/"+serverName+"_cert.pem"
        certFilePath = "/clientCert/"+serverName+"_cert.pem"
        #keyFilePath = os.environ['HOME']+"/clientKey/"+serverName+"_key.pem"
        keyFilePath = "/clientKey/"+serverName+"_key.pem"
        #caFilePath = os.environ['HOME']+"/serverCA/"+serverName+"_CA.pem"
        caFilePath = "/serverCA/"+serverName+"_CA.pem"
        with open(certFilePath,"w") as f1, open(keyFilePath,"w") as f2 , open(caFilePath,"w") as f3:
            f1.write(arguments["client_key"])
            f2.write(arguments["client_cert"])
            f3.write(arguments["KMIPCA_cert"])
        f1.close()
        f2.close()
        f3.close()
        return True
    except:
        #print("Error in saving certificate files")
        return False
    
#************************************************************************

#---Inf: update the server config files
#---Par: user arguments from panansas application 
#---Ret: success: True  ,  Failure : False
def updateServerConfFile(arguments):
    try:
        global ServerCfgData
        serverName = arguments["kmip_servername"]
        serverData={"kmip_address":arguments["kmip_address"],"port":str(SERVER_PORT),"reconnect":str(SERVER_RECONNECT),"client_cert":"/clientCert/"+serverName.lower()+"_cert.pem","client_key":"/clientKey/"+serverName.lower()+"_key.pem","KMIPprotocol":arguments["KMIPprotocol"],"nbio":str(SERVER_NBIO),"timeout":str(SERVER_TIMEOUT),"KMIPCA_cert":"/serverCA/"+serverName.lower()+"_CA.pem"}
        data={serverName:serverData}
        kmipDbData = [{"KMIP_Enabled":"False","KMIP_Batch_Mode":"False","KMIP_Client_CSR":"None","KMIP_Client_Private_Key":arguments["client_key"],"KMIP_Client_Public_Key":arguments["client_cert"],"KMIP_Server_Name":arguments["kmip_servername"],"KMIP_Server_Host_Address":arguments["kmip_address"],"KMIP_Server_CA_Cert":arguments["KMIPCA_cert"],"KMIP_Version":arguments["KMIPprotocol"]}]
        ret = createDatabaseConfigTable(kmipDbData)
        if ret is False:
           return False,"Error in Creating Config Database File"
        if os.path.exists("p6kmiptool.conf"):
            if os.path.getsize("p6kmiptool.conf") > 0:
                with open("p6kmiptool.conf","r") as r:
                    getOldData=r.read()
                r.close()
                ServerCfgData=json.loads(getOldData)
                ServerCfgData.append(data)
            else:
                ServerCfgData.append(data)

            with open("p6kmiptool.conf","w") as f:
                f.write(json.dumps(ServerCfgData))
            f.close()
            return True,"The server config file and database is created and updated"
        else:
            ServerCfgData.append(data)
            with open("p6kmiptool.conf","w") as f:
                f.write(json.dumps(ServerCfgData))
            f.close()
            return True,"The server config file and database is created and updated"
    except:
        #print("Error in writing Server config file")
        return False, "Error in creating Database and server config file"
    
#************************************************************************

#---Inf: Add the client Identity
#---Par: user arguments from panansas application 
#---Ret: Status of command execution and result in json
def addClientIdentity(arguments):
    ret1 = saveServerCertFiles(arguments)
    ret2,resp = updateServerConfFile(arguments)
    
    status = ""
    error = ""
    result = ""

    if (ret1 is True) and (ret2 is True):
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in Writing server config file"
        result= resp

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}
    
    return json.dumps(resp)

#************************************************************************

#---Inf: get the data from server config files
#---Par: None
#---Ret: success: True  ,  Failure : False and data from file
def getKMIPConfigData():
    try:
        if os.path.exists("p6kmiptool.conf"):
            if os.path.getsize("p6kmiptool.conf") > 0:
                with open("p6kmiptool.conf","r") as r:
                    getData=r.read()
                r.close()
                res=json.loads(getData)
                return True,res
            else:
                return False,None
        else:
            return False,None
    except:
        #print("Error in writing Server config file")
        return False,None

#************************************************************************

#---Inf: print the KMIP details in p6kmiptool.conf
#---Par: None
#---Ret: KMIP details in the config file
def printKMIPConfig():
    status = ""
    error = ""
    result = ""
    ret,resp = getKMIPConfigData()

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in getting server config data"
        result= ''

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    return json.dumps(resp)
#************************************************************************

#---Inf: update the server config file and update dataBase
#---Par: user arguments from panansas application 
#---Ret: success: True  ,  Failure : False
def getUpdateKMIPServer(arguments):
    try:
        
        if "kmip_servername" in arguments:
            serverName = arguments["kmip_servername"]
        else:
            serverName = None
        nameChk = 0
        if serverName:
          if os.path.exists("p6kmiptool.conf"):
            if os.path.getsize("p6kmiptool.conf") > 0:
                with open("p6kmiptool.conf","r") as r:
                    getOldData=r.read()
                r.close()
                serverData=json.loads(getOldData)
                NewServerData = []
                for data in serverData:
                  for (k,v) in data.items():
                      if serverName.lower() == k.lower():
                        nameChk = 1
                        for (subk,subv) in v.items():
                           if subk in arguments:
                               v[subk] = arguments[subk]
                  NewServerData.append(data)
                if nameChk == 1:
                    with open("p6kmiptool.conf","w") as f:
                        f.write(json.dumps(NewServerData))
                    f.close()
                else:
                    return False,"server Name doesn't exist"
                ret = updateDatabaseConfigTable(arguments)
                if ret is True:
                    return True,"The Server config File and Database is updated"
                else:
                    return False, "The Server Config Database is not updated"

                #print(serverData)
            else:
                return False,"The server config File is empty"
          else:
              return False,"The server file did not exist"
        else:
            return False,"The server Name is not Provided"
    except:
        #print("Error in writing Server config file")
        return False,""

#************************************************************************

#---Inf: update the KMIP details in file and in database
#---Par: kmip config data to be updated
#---Ret: status of updating KMIP data
def updateKMIPServer(arguments):
    status = ""
    error = ""
    result = ""
    ret,resp = getUpdateKMIPServer(arguments)

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in updating KMIP server config data"
        result= resp

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    return json.dumps(resp)
#************************************************************************

#---Inf: delete the server data in config file and in dataBase
#---Par: user arguments from panansas application 
#---Ret: success: True  ,  Failure : False
def getDeleteKMIPServer(arguments):
    try:

        if "kmip_servername" in arguments:
            serverName = arguments["kmip_servername"]
        else:
            serverName = None
        nameChk = 0
        if serverName:
          if os.path.exists("p6kmiptool.conf"):
            if os.path.getsize("p6kmiptool.conf") > 0:
                with open("p6kmiptool.conf","r") as r:
                    getOldData=r.read()
                r.close()
                serverData=json.loads(getOldData)
                chkflag=0 
                for i in range(len(serverData)):
                    for (k,v) in serverData[i].items():                    
                        if serverName.lower() == k.lower():
                            chkflag = 1
                            del serverData[i]
                    if chkflag == 1:
                        break
                    else:
                        continue
                with open("p6kmiptool.conf","w") as f:
                    f.write(json.dumps(serverData))
                f.close()
                
                ret = delDatabaseConfigTable(arguments)
                if ret is True and chkflag == 1:
                    return True,"The Server config File Data is deleted from Database "
                else:
                    return False, "The Server Config Database is not deleted"

            else:
                return False,"The server config File is empty"
          else:
              return False,"The server file did not exist"
        else:
            return False,"The server Name is not Provided"
    except:
        #print("Error in writing Server config file")
        return False,""

#************************************************************************

#---Inf: Delete the KMIP details in file and in database
#---Par: kmip server name to be deleted
#---Ret: status of deletion KMIP data
def deleteKMIPServer(arguments):
    status = ""
    error = ""
    result = ""
    ret,resp = getDeleteKMIPServer(arguments)

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in Deleting KMIP server config data"
        result= resp

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    return json.dumps(resp)

#************************************************************************

#---Inf: Get the Client Credentials
#---Par: user arguments from panansas application 
#---Ret: True/False and CSR data 
def getClientCredentials(arguments):
    #try:
        comName = None
        comName = arguments["name"].replace(" ","").lower()
    
        if comName:
            #Process to generate CSR
            getKmip = X509Certificate()
            getKmip.getAPI(json.dumps(arguments))
            csrData={"CSRpath":CSRFILE+"/"+comName+".csr","KeyPath":CSRFILE+"/"+comName+".key"}
            return True,csrData
        else:
            return False,"Provide Common Name for the Client"
    #except:
        #return False,""

#************************************************************************

#---Inf: Generate Client Credentials
#---Par: user arguments from panansas application 
#---Ret: Status of command execution and result in json
def generateClientCredential(arguments):
    status = ""
    error = ""
    result = ""

    ret,resp = getClientCredentials(arguments)
    
    
    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in generating CSR Files"
        result= resp

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    return json.dumps(resp)

#************************************************************************

#---Inf: Get CSR
#---Par: user arguments from panansas application 
#---Ret: True/False and CSR data from the saved files
def getCsr(arguments):
    #try:
        if arguments:
            comName = ""
            comName = arguments["name"].replace(" ","").lower()
            if comName:
                clientFilePath = CSRFILE+'/'+comName+'.csr'
                if os.path.exists(clientFilePath):
                    with open(clientFilePath,"r") as f1:
                        csr = f1.read()
                    f1.close()
                    csrData = {"CN":arguments["name"],"CSR":csr}
                    return True,csrData
                else:
                    return False,"Provided Common Name CSR didn't exist, Generate Client Credentials to get CSR"
            else:
                return False,"Provide Common Name for CSR"
        else:
            updateData =[] 
            for filename in os.listdir(CSRFILE):
                if ".csr" in filename:
                    clientFilePath = CSRFILE+'/'+filename
                    with open(clientFilePath,"r") as f:
                        csr = f.read()
                    f.close()
                    data={"CN":filename.split(".")[0],"CSR":csr}
                    updateData.append(data)
            return True,updateData
    #except:
        #return False,""

#************************************************************************
#---Inf: Print CSR for the common name 
#---Par: user arguments from panansas application 
#---Ret: Status of command execution and result in json
def printCSR(arguments):
    status = ""
    error = ""
    result = ""
    
    if arguments is not None:
        ret,resp = getCsr(arguments)
    else:
        ret,resp = getCsr()


    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in reading CSR File"
        result= resp

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    return json.dumps(resp)

#************************************************************************

#---Inf: Get the server Data
#---Par: user arguments from panansas application
#---Ret: True/False and Server data
def getServerQuery(arguments):
    SerName = None
    SerName = arguments["server"]
    try:
        if SerName:
            #Process to generate CSR
            Data = "Server Query Data"

            serData = {"Server":SerName,"Data":Data}
            return True,serData
        else:
            return False,None
    except:
        return False,None

#************************************************************************

#---Inf: Get the server Query data
#---Par: user arguments from panansas application
#---Ret: Status of command execution and result in json
def queryServer(arguments):
    status = ""
    error = ""
    result = ""

    ret,resp = getServerQuery(arguments)

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in getting Server Query Data"
        result= ''

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    return json.dumps(resp)
#************************************************************************

#---Inf: Get the server Verify Data
#---Par: user arguments from panansas application
#---Ret: True/False and Server Verify data
def getServerVerify(arguments):
    SerName = None
    SerName = arguments["server"]
    try:
        if SerName:
            #Process to generate CSR
            #Data = "Server Query Data"

            serData = {"Authentication": "Pass", "AES_Key": "Pass", "Modify_Key":"Pass", "Activate_Key":"Pass", "Retrieve_Key_Attr":"Pass", "Retrieve_Raw_Key":"Pass", "Locate_Key":"Pass","Revoke_Key":"Pass","Destroy_Key":"Pass","Test_Batch_Support":"Pass"}
            return True,serData
        else:
            return False,None
    except:
        return False,None
#************************************************************************
#---Inf: Verify the Server
#---Par: user arguments from panansas application
#---Ret: Status of command execution and result in json
def verifyServer(arguments):
    status = ""
    error = ""
    result = ""

    ret,resp = getServerVerify(arguments)

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in getting Verify Server Data"
        result= ''

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    return json.dumps(resp)

#************************************************************************

#---Inf: Get the server Status
#---Par: user arguments from panansas application
#---Ret: True/False and Server Verify data
def getServerStatus():
    try:
            #Process to generate CSR
            #Data = "Server Query Data"

            serData = [{"Client": "commonname", "ServerName": "Server1", "IPAddress": "10.1.2.3", "Status": "OK", "BatchMode": "Enabled"},{"Client": "commonname", "ServerName": "Server2", "IPAddress": "10.1.2.4", "Status": "OK", "BatchMode": "Enabled"}]
            return True,serData
    except:
        return False,None
#************************************************************************

#---Inf: Check the Server Status
#---Par: user arguments from panansas application
#---Ret: Status of command execution and result in json
def checkStatus():
    status = ""
    error = ""
    result = ""

    ret,resp = getServerStatus()

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in getting KMIP Server Status"
        result= ''

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    return json.dumps(resp)

#************************************************************************

#---Inf: Set the Batch Mode
#---Par: user arguments from panansas application
#---Ret: True/False and batch mode status
def setBatchMode(arguments):
    mode = None
    mode = arguments["mode"]
        
    try:
        if "enable" in mode:
            #Process to generate CSR
            #Data = "Server Query Data"

            serData = {"mode": "Enabled"}
            return True,serData
        elif "disable" in mode:
            serData = {"mode": "Disabled"}
            return True,serData
        else:
            return False,None
    except:
        return False,None
#************************************************************************
#---Inf: Configure the Batch Mode
#---Par: user arguments from panansas application
#---Ret: Status of command execution and result in json
def configBatchMode(arguments):
    status = ""
    error = ""
    result = ""

    ret,resp = setBatchMode(arguments)

    if ret is True:
        status = "Success"
        error  = "None"
        result = resp
    else:
        status = "Failure"
        error = "Error in Setting Batch Mode"
        result= ''

    resp= {"STATUS" : status, "ERROR": error, "RESULT": result}

    return json.dumps(resp)





