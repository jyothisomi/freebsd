#!/usr/bin/env python3
# Logging
# pickup configuration files from yaml and utils/configs
#
#

import logging
from datetime import datetime
#from utils import yaml_config
from yaml_config import *


#logCfg = yaml_config.YAMLConfig.getYAMLConfig()
logCfg = YAMLConfig.getYAMLConfig()
logDir = logCfg['log_dir']
logFile = logCfg['log_file']
logLevel = logCfg['log_level']

def customTime(*args):
    utc_dt = datetime.utcnow()
    return utc_dt.timetuple()


logging.Formatter.converter = customTime

logging.basicConfig(filename=logFile,
                    format='%(asctime)s  %(levelname)-10s %(processName)s  %(name)s %(message)s')

# Creating an object
logger = logging.getLogger()

# Setting the threshold of logger to DEBUG
if logLevel == 'DEBUG':
    logger.setLevel(logging.DEBUG)

if logLevel == 'INFO':
    logger.setLevel(logging.INFO)

if logLevel == 'WARNING':
    logger.setLevel(logging.WARNING)

if logLevel == 'ERROR':
    logger.setLevel(logging.ERROR)


def log_msg(msgType, msg):
    if msgType is "DEBUG":

        logger.debug(msg)

    elif msgType is "INFO":

        logger.info(msg)

    elif msgType is "WARNING":

        logger.warning(msg)

    elif msgType is "ERROR":

        logger.error(msg)

    else:

        logger.exception("Wrong UserInput Log Format Type")
