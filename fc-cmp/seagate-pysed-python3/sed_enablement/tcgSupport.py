tokens_table= {'PIN':              3,
              'RangeStart':        3,
              'RangeLength':       4,
              'ReadLockEnabled':   5,
              'WriteLockEnabled':  6,
              'ReadLocked':        7,
              'WriteLocked':       8,
              'LockOnReset':       9,
              'Enabled':           5}

def tokens(sed):
    '''
    The function to create the access handles for the tcgapi.
    
    Parameters:
        sed - SED device structure
    
    Returns:
        Returns numbered access specifier token in case of Opal and empty list in case of Enterprise.
    '''
    
    if sed.SSC== 'Enterprise':
        return []
    elif sed.SSC == 'Opalv2':
        token_values = [1,[]]
        for key,val in sed.token.items():
            token_values[1].append((tokens_table[key],val))
        sed.token.clear()
        sed.token.update({'noNamed':True})
        return (tuple(token_values))
        
    
    