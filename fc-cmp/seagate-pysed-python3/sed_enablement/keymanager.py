class KeyManager(object):
    '''
    This is a class to store authority and credentials temporarily.
    '''
    
    def __init__(self):
        '''
        The function to create a structure for authority and credentials.
        '''
        self.credentials = {}
    
    def getKey(self,auth):
        '''
        The function to get the credential value for an authority.
        
        Parameters:
            auth -Authority
        Returns:
            cred - credential
        '''
        
        cred = self.credentials[auth]
        return cred
        
    def setKey(self,auth,cred):
        '''
        The function to set credential for an authority.
        
        Parameters:
            auth - Authority
            cred - credential
        '''
        
        self.credentials[auth]=cred
        return 