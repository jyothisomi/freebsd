//-----------------------------------------------------------------------------
//
// Do NOT modify or remove this copyright and confidentiality notice!
//
// Copyright (c) 2016 - Seagate Technology, LLC.
//
// The code contained herein is CONFIDENTIAL to Seagate Technology, LLC.
// Portions are also trade secret. Any use, duplication, derivation, distribution
// or disclosure of this code, for any reason, not expressly authorized is
// prohibited. All other rights are expressly reserved by Seagate Technology, LLC.
//
//-----------------------------------------------------------------------------

#ifndef TCGSCANNER_H_
#define TCGSCANNER_H_
#include <stddef.h>
#include <list>
//#pragma GCC diagnostic warning "-Wunused-local-typedefs"
#include <boost/python.hpp>
#include <boost/python/object.hpp>
#include <boost/python/dict.hpp>
#include <boost/python/tuple.hpp>
#include <boost/python/list.hpp>
#include <boost/python/str.hpp>
#include "TcgDrive.h"
#include "parser.tab.hpp"

namespace Tcg {
using boost::python::list;
using boost::python::dict;

class RcvdDataList;

/*
class ParserValue
{

	uint64_t	intValue;
	std::string	strValue;
	enum pvType {
		pvNone,
		pvInteger,
		pvString,
		pvList,
	}			type;
	std::string name;
	std::list<ParserValue> listValue;
public:
	ParserValue();
	ParserValue(uint8_t	* atomData, unsigned atomLength, bool b);

	bool isString() {return type == pvString;}
	bool isInt()	{return type == pvInteger;}
	bool isNone()	{return type == pvNone;}
	bool isList()	{return type == pvList;}
	void setList()  {type = pvList;}
	operator std::string();
	operator uint64_t();
	void add(ParserValue & val);
	void setName(std::string _name) {name = _name;}
	ParserValue & find(const char * name);
};
*/

// Lexer to decode tokens from the TCG received packets
class Scanner
{
	RcvdDataList &		data;
	uint8_t *			start;
	uint8_t *			next;
	uint8_t * 			end;
	size_t 				offset;

	Parser::symbol_type nextToken();

protected:

public:
	Scanner(RcvdDataList & _data);
	~Scanner();

	Parser::symbol_type get_next_token();
	size_t getPos() {return offset + next - start;}
	std::string getBuffer();
	bool amDone();
};

struct ParserAbort {
	StatusCode 	rv;
	ParserAbort(StatusCode _rv) : rv(_rv) {}
};

class Results {
	int		resultCode;

	bool findDict(list l);
public:
	dict	returnedNamedValues;
	list		returnedValues;

	void setResultCode(long_& val);
	void setResultCode(ParserAbort & pa) {resultCode = pa.rv;}
	void setReturnedValues(object & val);
	int getResultCode() {return resultCode;}	// Get execution status (result code after EndData token)
	int getReturnCode(int index = 0);			// Get return code from return value stack
	bool findObject(const char * name, object & ob);
	bool findObject(const uint64_t name, object & ob);
	void convertNamedList(list & l, object & results);

	// retrieve named string result from return values
	const char * namedString(const char * name);
	const char * namedString(const uint64_t name);
	// retrieve named integer result from return values
	uint64_t namedValue(const char * name, uint64_t defaultValue = 0);
};

}; // namespace Tcg

#endif /* TCGSCANNER_H_ */
