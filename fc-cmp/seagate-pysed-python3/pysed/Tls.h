//-----------------------------------------------------------------------------
//
// Do NOT modify or remove this copyright and confidentiality notice!
//
// Copyright (c) 2017 - Seagate Technology, LLC.
//
// The code contained herein is CONFIDENTIAL to Seagate Technology, LLC.
// Portions are also trade secret. Any use, duplication, derivation, distribution
// or disclosure of this code, for any reason, not expressly authorized is
// prohibited. All other rights are expressly reserved by Seagate Technology, LLC.
//
//-----------------------------------------------------------------------------

/*
 * Tls.h
 *
 *  Constants and structures used for TLS.
 *
 *  Created on: June 5, 2017
 *      Author: john
 */

#ifndef PYSED_TLS_H_
#define PYSED_TLS_H_
#if !defined(_WINDOWS)
#include <gnutls/gnutlsxx.h>
#endif
#include "TcgDrive.h"
#include <vector>

namespace Tcg {

struct TlsPacketHeaders {
	ComPacketHeader		comPacket;
	PacketHeader 		packet;

	void fill(Drive * const drive, Session * const session, uint32_t dataLength);
};

#if defined(_WINDOWS)
#pragma pack(push, 1)
#endif

struct TlsHeader {
	uint8_t		contentType;
	uint8_t		version[2];
	beint16_t	length;
}  __attribute__((__packed__));

#if defined(_WINDOWS)
#pragma pack(pop)
#endif

class SupportedSuites
{
	std::vector<unsigned> 	suites;
	std::vector<int>		indices;
public:
	SupportedSuites();
	bool supports(unsigned cipherSuite, int * cfgNdx = NULL);
	std::string cfgString(int index);
	unsigned index2cipherSuite(int index);
};

class TlsCredentials {
public:
	gnutls_psk_client_credentials_t	creds;
	TlsCredentials(std::string user, std::string psk);
	~TlsCredentials();
};

class TlsSession : public Session, private gnutls::session {
	static const size_t DataBlockExtra = 512;					// Extra room for TCG Headers rounded to a full sector
	static const size_t	DataBlockLen = 4096 + DataBlockExtra;	// Block size needs to be 2^N for gnutls
	static const int TlsStartPadBytes = 3;

	int			pullDataRead, pullDataWrite;		// FIFO to hold encrypted data from drive (without headers and pad)
	bool		tlsEstablished;						// true when handshake complete

protected:
	virtual void startSessionEstablished();
	void writeData(uint8_t	* tlsBuffer, uint32_t & tlsBufferLength);
	bool receiveData(unsigned timeout = 0, bool once = false);

public:
	TlsSession(Drive * const dr, Uid sp = 0, unsigned to = 0);
	~TlsSession();

	// TcgSession overrides
	virtual void sendPackets(bool endSession);
	virtual void close();
	virtual void getRcvdData(RcvdDataList & collect);

	static SupportedSuites & supportedSuites() {
		static SupportedSuites	supported;
		return supported;
	}

private:
	// gnutls hooks
	static ssize_t pushFunctionStub(gnutls_transport_ptr_t _self, const giovec_t * iov, int iovcnt)
	{
		TlsSession * self = reinterpret_cast<TlsSession *>(_self);
		return self->pushData(iov, iovcnt);
	}
	static ssize_t pullFunction(gnutls_transport_ptr_t h, void * buf, size_t len);
	static int pullTimeoutFunction(gnutls_transport_ptr_t _self, unsigned int ms);
	ssize_t pushData(const giovec_t * iov, int iovcnt);
	static void logFilter(int level, const char * msg);
	static void auditLog(gnutls_session_t session, const char * msg);
};

}; // Tcg namespace

#endif /* PYSED_TLS_H_ */
