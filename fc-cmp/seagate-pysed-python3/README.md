sed_enablement

sed_enablement currently provides support for linux and Windows 10 platforms. The C++ source is making use of the C++98 standard and is statically linked to opensea-libraries.

Dependencies

Install Python 3 and pip3.

Use pip3 to install the following package dependencies

Package        		--		    Version
---------------  			----------
asn1crypto      	--		    0.24.0

boost          		--			0.1

certifi         	--			2018.11.29

cffi            	--			1.11.5

chardet         	--		    3.0.4

cryptography    	--		    2.4.2

passlib         	--			1.7.1

pyasn1          	--			0.4.4

pycparser       --		    2.19


Building

Linux

To build open-sea libraries,

	python3 setup.py opensea

To build the pysed dynamic library:

	python3 setup.py build

The python package sed_enablement will be installed in the path build/lib.linux-x86_64-3.6/sed_enablement


---------------------------------------------------------------------------------------------------------
Changes made in the openseaChest and pysed to support json data

step1: sudo apt-get install libjson-c-dev
step2: change the Makefile of opensea-transport/Make/gcc/Makefile to support the json-c library
       CFLAGS += -c -fPIC -I/usr/include/json-c -std=gnu99
       LFLAGS ?= -Wall ../../../opensea-common/Make/gcc/lib/$(FILE_OUTPUT_DIR)/libopensea-common.a /usr/lib/x86_64-linux-gnu/libjson-c.a
step3: change the setup.py so that we can include the json libraries 
       libraries=['boost_python3', 'gnutls', 'gnutlsxx', 'json-c'],
       include_dirs = ['pysed','opensea-transport/include','opensea-common/include','opensea-operations/include','opensea-transport/include/vendor','/usr/include/json-c']

----------------------------------------------------------------------------------------------------
Changes made in the sed-enablements python code tcgapi.py to support our fcdms commands

step1: created new class and will updated in that class if there is no drive as parameter


 
