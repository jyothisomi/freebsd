#!/usr/bin/env python3
# Setuptools script that builds Seagate-sed-enablement
# Seagate-sed-enablement requires building the pysed C++ dynamic link library pysed.so
# Pysed requires the OpenSea Libraries: opensea-transport, opensea-common, and opensea-operations.
# Build the static libraries for opensea before building pysed. 
#
# To use the tool run python setup.py with the following argument options 
#
# To build the Opensea libraries:
#    python setup.py opensea
#
# To build the pysed dynamic library:
#    python setup.py build
#
# To build a seagate-sedutil linux rpm:
#    python setup.py bdist_rpm
#
#
from setuptools import setup, Extension
import subprocess

from setuptools import Command

class BuildOpenSea(Command):
    user_options = []
    
    def initialize_options(self):
        """Abstract method that is required to be overwritten"""

    def finalize_options(self):
        """Abstract method that is required to be overwritten"""
    def run(self):
        print(" => Building OpenSea Static Libraries ...")
        subprocess.call(['gmake', '-f', 'opensea-transport/Make/gcc/Makefile'])
        subprocess.call(['gmake', '-f', 'opensea-operations/Make/gcc/Makefile'])
        subprocess.call(['gmake', '-f', 'opensea-common/Make/gcc/Makefile'])


pysed = Extension('sed_enablement.pysed', [
        'pysed/pysed.cpp',
        'pysed/TcgDrive.cpp',
        'pysed/transport.cpp',
        'pysed/TcgScanner.cpp',
        'pysed/parser.tab.cpp',
        'pysed/support.cpp',
        'pysed/Tls.cpp',
    ],
    libraries=['boost_python3', 'gnutls', 'gnutlsxx', 'json-c'],
    include_dirs = ['pysed','opensea-transport/include','opensea-common/include','opensea-operations/include','opensea-transport/include/vendor','/usr/include/json-c'],
    extra_objects=['opensea-transport/Make/gcc/lib/libopensea-transport.a','opensea-common/Make/gcc/lib/libopensea-common.a','opensea-operations/Make/gcc/lib/libopensea-operations.a']
)

setup(
    name='Seagate-sed-enablement',
    description='Seagate sed_enablement utility.',
    long_description='Seagate sed_enablement utility.\nProvides support for taking ownership of the drive, configuring bands, locking and unlocking. Includes support for SAS, SATA interfaces via OpenSea-libraries,\nSED support for Enterprise(Full), OpalV2(Limited)',
    version='1.0',
    packages=['sed_enablement'],
    ext_modules=[pysed],
    url = 'https://gitlab.com/futura-cyber/seagate-pysed',
    author = 'Seagate Technology LLC',
    cmdclass={
        'opensea': BuildOpenSea,
    }

)


