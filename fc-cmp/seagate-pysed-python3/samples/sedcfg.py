import os
from os import path
import sys
import logging
import logging.handlers
import argparse
import struct
sys.path.append( path.dirname( path.dirname( (__file__) ) ) )
from sed_enablement.tcgapi import Sed, StatusCode
import sed_enablement.keymanager as keymanager

class Sedcfg(object):
    
    """
    This is a class for performing band operations on the SED drive
    
    Attributes:
        dev: Device handle of the drive.
    """
    
    cred_table = {
        'SID':          'ADMIN',
        'C_PIN_Admin1': 'ADMIN1',
        'Admin1':       'ADMIN1',
        'C_PIN_User1':  'USER1',
        'User1'      :  'USER1',
        'User2'      :  'USER2',
        'C_PIN_User2':  'USER2',
        'EraseMaster':  'ERASEMASTER',
        'BandMaster1':  'BANDMASTER1',
        'BandMaster2':  'BANDMASTER2'
    }
        
    def __init__(self, dev):
        '''
        The constructor for Sedcfg class.
        
        Parameters:
            dev:Device handle of the drive.
        '''
        
        os_type = {'linux2':self.linux_platform, 'win32':self.windows_platform}
        os_type[sys.platform](dev)
        
        logging.basicConfig(
            filename=self.log_filename,
            format="%(asctime)s %(name)s (%(threadName)s) - %(message)s",
            level=logging.DEBUG
        )
        self.logger = logging.getLogger(self.log_filename)
        self.logger.debug('Start sedcfg Logger')
        sed_values = {'logger':self.logger}
            
        # Build the SED object for the drive
        self.sed = Sed(dev,callbacks = self,**sed_values)
        
        self.keymanager = keymanager.KeyManager()
        for key,val in self.cred_table.items():
            self.keymanager.setKey(key,val)
        
        self.BandLayout = sedbandlayout()
        self.BandLayout.bandauth(self)
        self.sed.token = {}
        self.initial_cred = self.sed.mSID
                
    def linux_platform(self, devname):
        '''
        The function to initialize parameters for the linux platorm.
        
        Parameters:
            devanme:Device handle of the drive.
        '''
        
        self.log_filename = os.path.join(os.path.dirname(__file__), 'sedcfg.log')   
        self.devname = devname 
    
    def windows_platform(self, devname):
        '''
        The function to initialize parameters for the windows platorm.
        
        Parameters:
            devanme:Device handle of the drive.
        '''
        
        if getattr(sys, 'frozen', False):
            # frozen
            self.log_filename = os.path.join(os.path.dirname(sys.executable), 'sedcfg.log')
        else:
            # unfrozen
            self.log_filename = os.path.join(os.path.dirname(__file__), 'sedcfg.log')
        
        # For Windows we need to modify the input value from PD to the physical volume
        # Extract PD from string and take the number value to be used and extrapolate into \\.\PhysicalDrive#

        if ("PD" not in devname): 
            print "Please pass drive in as PD<drive number>"
            print "Example: Disk 1 is PD1"
            exit (1)
            
        drive_number = devname[-1:]
        self.devname = "\\\\.\\PhysicalDrive" + drive_number
            
    def take_ownership(self,args=None):
        '''
        The function to take owenership of the drive by changing default Admin credentials, to create band authorities and changing
        credentials of the created band authorities. 
        
        Parameters:
            args - Commandline arguments.
            
        Returns:
            True: Successful completion of taking drive ownership.
            False: Failure of taking drive ownership.
        
        '''
        self.logger.debug('Taking ownership of the drive')
        # Change PIN of Admin to a new PIN from default value
        good = self.sed.changePIN(self.BandLayout.authority[0],self.keymanager.getKey(self.BandLayout.authority[0]),(None,self.initial_cred))
        if good is True:
            if self.BandLayout.authority[1] is 'Admin1':
            # Activate the Locking SP of the drive only for OPAL case
                if self.sed.activate(self.BandLayout.authority[0]) == False:
                    return False   
                self.initial_cred = self.getCred('SID')
            # Change PIN of Admin of Locking SP
            if self.sed.changePIN(self.BandLayout.authority[1], self.keymanager.getKey(self.BandLayout.authority[1]),(None,self.initial_cred), self.BandLayout.auth_objs[0]) == False:
               return False
            if self.enable_authority() is True:
                print 'Credentials of the drive are changed successfully'
                return True
        return False   
             
    def enable_authority(self):
        '''
        The function to enable authorities and change their credentials.
        
        Returns:
            True: Enable Authority successfull.
            False: Failure to Enable Authority.
        
        '''
        self.logger.debug('Enable Authority on the drive')
        # Enable two users User1 and User2 and change their password to USER1 and USER2, Bandmaster1 is enabled by default in case of Enterprise.
        for obj in self.BandLayout.auth_objs[3:]:
            if self.sed.enableAuthority(self.BandLayout.authority[1], True, obj) is True:
                continue
            else:
                return False
        # Change pin of band authorities to a new value
        for (obj, auth) in zip(self.BandLayout.auth_objs[1:], self.BandLayout.authority[2:]):
            if self.BandLayout.authority[1] is 'Admin1':
                auth = 'Admin1'
                self.initial_cred = self.keymanager.getKey(auth)
            if self.sed.changePIN(auth, self.keymanager.getKey(obj),(None,self.initial_cred),obj) == False:
                return False
            else:
                continue
        return True
        
    def configure_bands(self, args):
        '''
        The function to configure bands on the drive and assign bands to authorities. 
        
        Parameters:
            args - Commandline arguments:
                   Bandno: Bandnumber to be configured
                   RangeStart: RangeStart value
                   Rangelength:Rangelength value
                   LockOnReset: True or False
        
        Returns:
            True: Successfull completion of configuring bands.
            False: Failure to configure bands.
        '''
        self.logger.debug('Configuring bands on the drive')
        # Enable band and set ranges for band
        if self.BandLayout.authority[1] is 'Admin1':
            auth = 'Admin1'
        else:
            auth = 'BandMaster' + args.Bandno
        if args.LockOnReset == str(True):
            args.LockOnReset = [0]
        else:
            args.LockOnReset = []
        configure = self.sed.setRange(auth, int(args.Bandno), authAs=(auth, self.keymanager.getKey(auth)), RangeStart=int(args.RangeStart), RangeLength=int(args.RangeLength),
                                      ReadLockEnabled=1, WriteLockEnabled=1, LockOnReset=args.LockOnReset,
                                      ReadLocked=0, WriteLocked=0)
        if auth is 'Admin1' and configure is True:
        # Give access to users to read and write unlock range only in OPAL case, Bands are assigned to authorities by default in case of Enterprise.
            range_objs = ['ACE_Locking_Range1_Set_RdLocked', 'ACE_Locking_Range1_Set_WrLocked',
             'ACE_Locking_Range2_Set_RdLocked', 'ACE_Locking_Range2_Set_WrLocked']
            if args.Bandno == '1':
                range_obj = range_objs[:2]  
            else:
                range_obj = range_objs[2:]
            for objts in range_obj:
                ret = self.sed.enable_range_access(objts, 'User' + args.Bandno, auth)
                if ret == False:
                    return False
        if configure == True:
            print 'Band{} is configured successfully'.format(args.Bandno)
            return True
        return False
        
    def lock_unlock_bands(self, args):
        '''
        The function to lock and unlock the bands present on the drive
        
        Parameters:
            args - Command line arguments:
                   lock/unlock: Lock/Unlock the band
                   bandno: Bandnumber
                   
        Returns:
            True : Successfull completion of the operation.
            False: Failure of the operation
        '''
        self.logger.debug('Locking/Unlocking bands on the drive')
        if self.BandLayout.authority[1] is not 'Admin1':
            # Using getRange for EntSSC case, support needs to be added for OPAL
            Range_info = self.sed.getRange(int(args.bandno))
            print "Band state before lock/unlock =\n{}".format(Range_info)
            auth = 'BandMaster' + args.bandno 
        else:
            auth = 'User' + args.bandno 
        if(args.lockunlock == "lock"):
            lock_unlock = 1
            if self.BandLayout.authority[1] is not 'Admin1':
                if (Range_info.ReadLocked == 1):
                    print "Band{} already in locked state".format(args.bandno)
                    return True
        elif(args.lockunlock == "unlock"):
            lock_unlock = 0
            if self.BandLayout.authority[1] is not 'Admin1':
                if (Range_info.ReadLocked == 0):
                    print "Band{} already in unlocked state".format(args.bandno)
                    return True
        # Perform a lock-unlock on the range
        lock_unlock = self.sed.setRange(auth, int(args.bandno), authAs=(auth, self.keymanager.getKey(auth)), ReadLocked=lock_unlock, WriteLocked=lock_unlock)
        if lock_unlock == True:
            print "Band{} {}ed successfully by {}".format(args.bandno, args.lockunlock, auth)
            if self.BandLayout.authority[1] is not 'Admin1':
                print self.sed.getRange(int(args.bandno))
            return True
        return False
        
    def erase_drive(self, args):
        '''
        The function to revert the drive back to factory state.
        
        Parameters:
            args - Commadline arguments.
                   psid: PSID number of the drive
        
        Returns:
            True : Successfull completion of the operation.
            False: Failure of the operation
        '''
        self.logger.debug('Erasing the drive')
        result = self.sed.revert(args.psid)
        if (result == True):
            print "Drive successfully reverted back to factory state"
        else:
            print "Failed to revert drive to factory state"
        return result
    
    def getCred(self, auth):
        '''
        The function to get credential from the key manager class for the authority.
        
        Parametrs:
            auth - Authority for which a credential is needed.
            
        Returns:
            The credential for the authority.
        
        '''
        self.logger.debug('Obtaining Credentials for the authority')
        return self.keymanager.getKey(auth)
    
    def failedCred(self, auth, cred):
        '''
        Callback from tcgapi to deal with wrong credential for an authority.
        
        Parameters:
            auth - authroity
            cred - Credential for the authority.
        '''
        msg = 'Invalid credentials'
        self.logger.error(msg)
    
    def getAuth(self,op,defauth=None):
        '''
        Callback from tcgapi to obtain the authority for the operation being performed.
        
        Parameters:
            op - operation being performed
            defauth - Default authority for the operation.
        '''
        
        if defauth is None:
            msg = 'Please provide the authority for'+''+op        
            self.logger.error(msg)
        return 

    def fail(self, msg=None, op=None, status=None):
        '''
        Callbacks from tcgapi in case of failure of operations
        
        Parameters;
            msg - Message to be logged
            op  - Operation being performed
            status - Status of the operation being performed.
        '''
        
        lmsg = ''
        lmsg += self.devname + ' '
        if isinstance(msg, basestring):
            lmsg += msg + '\n'
        if op is not None:
            lmsg += 'Failed SED operation {0}: {1} '.format(op, StatusCode.values[status])
        self.logger.error(lmsg)
    
class sedbandlayout(object):
    '''
    This a class defining the band Layout of the drive.
    '''
    # Class can be modified to add multiple users in a dynamic fashion
    def __init__(self):
        
        '''
        The function defines parameters for the BandLayout of the drive.
        '''
        self.Ent_auth = ['SID', 'EraseMaster', 'BandMaster1', 'BandMaster2']
        self.Opal_auth = ['SID', 'Admin1', 'User1', 'User2']
        self.Ent_objs = ['EraseMaster', 'BandMaster1', 'BandMaster2', 'C_PIN_BandMaster1', 'C_PIN_BandMaster2']
        self.Opal_objs = ['C_PIN_Admin1', 'C_PIN_User1', 'C_PIN_User2', 'User1', 'User2']
        self.enabledbands = ['BandMaster0']
        
    def bandauth(self, sedbandcfg):
        '''
        The function to choose between Enterprise and Opal band layout.
        '''
        if sedbandcfg.sed.SSC == 'Enterprise':
            self.authority = self.Ent_auth
            self.auth_objs = self.Ent_objs
        else:
            self.authority = self.Opal_auth
            self.auth_objs = self.Opal_objs

class argParser(object):
    '''
    This is a class to parse the command line arguments.
    '''
    prog = 'sedbandcfg'
    description = 'Script depicting taking ownership of the drive,enabling users,setting up and locking,unlocking bands'
   
    def getParser(self):
        '''
        The Function to parse command line arguments and initialse operations.
        '''

        main = self.main = argparse.ArgumentParser(
            prog=self.prog,
            description=self.description,
        )
        
        main.add_argument('device', help='Specific wwn or device names of drives to operate on')
        subparser = main.add_subparsers(title='subcommand')
        revert = subparser.add_parser('revert', help='Revert the drive back to factory state')
        revert.add_argument('psid', help='PSID of the drive used to revert the drive back to factory state')
        revert.set_defaults(operation=Sedcfg.erase_drive)
        changecreds = subparser.add_parser('changecreds', help='Change the drive default credentials')
        changecreds.set_defaults(operation=Sedcfg.take_ownership)
        configure = subparser.add_parser('configure', help='Configure the bands by setting new band ranges')
        configure.add_argument('Bandno', help='Band number to configure')
        configure.add_argument('RangeStart', help='Rangestart value, Default(4097)')
        configure.add_argument('RangeLength', help='RangeLength value, Default(219749770)')
        configure.add_argument('LockOnReset', help='True or False value for LockOnReset')
        configure.set_defaults(operation=Sedcfg.configure_bands)
        bandops = subparser.add_parser('bandops', help='Perform a lock or an unlock on the band')
        bandops.add_argument('lockunlock', help='Lock, Unlock the band')
        bandops.add_argument('bandno', help='band number to be locked unlocked')
        bandops.set_defaults(operation=Sedcfg.lock_unlock_bands)
        return main
    
    def doParse(self, args):
        '''
        The function to obtain arguments.
        '''
        if args is not None:
            args = shlex.split(args)
        else:
            args = sys.argv[1:]
        namespace = self.getParser().parse_args(args)
        
        return namespace

def main(args=None):
    drive_namespace = argParser().doParse(args)
    sedcfg = Sedcfg(drive_namespace.device)
    rv = drive_namespace.operation(sedcfg, drive_namespace)
    if rv is not True:
        print "Operation failed"
        return 1

if __name__ == "__main__":
    sys.exit(main())
