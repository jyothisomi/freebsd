sedcfg is an example python script used to perform operations invloving taking ownership of the drive, enable users,configure bands and reverting the drive back to factory state. 

The credentials for the Admin and Users are statically programmed in the example script. The module keymanager.py can be used to support external keymanagers. 

sedcfg depicts the invocation of the tcgapi.py module which is used to communicate with the underlying pysed shared library. 
The script currently works with 2 bands but can be modified to add additional bands and Users.   

The script needs to be run as Administrator/root.On the linux system, to allow the security commands to reach the SATA drives, set the below flag value to 1.

		sys/module/libata/parameters/allow_tpm

1.Taking ownership of the drive by changing credentials,

	Usage:    python sedcfg.py <device> <operations>
	Eg:      1. (Linux)  python sedcfg.py /dev/sd? changecreds 
	         2. (Windows)python sedcfg.py PD? changecreds
 
(Note:This results in change of default Admin password of the drive.Hence, trying to perform the operation for the second time on the drive 
	  without a revert results in the failure of the operations on the drive. 

2.To configure bands on the drive:

	Usage:   python sedcfg.py <device> <operations> <flags>
	Eg:    1.python sedcfg.py /dev/sd? configure 1 8 64 True
	       2.python sedcfg.py /dev/sd? configure 2 80 88 False	
	       
(Instructions on setting band values:  
1. For the RangeStart input a value that is a multiple of 8 to maintain sector alignment.
2. Rangelength cannot exceed the maximum sector size of the drive
3. Maintain difference between 2 band ranges. In other words band ranges cannot overlap)

3.To lock,unlock the configured bands.

	Usage:	 python sedcfg.py <device> <operations> <flags>
	Eg:	   1.python sedcfg.py /dev/sd? bandops lock 1
		2.python sedcfg.py /dev/sd? bandops unlock 2

4.To revert the drive back to factory state:

	Usage: python sedcfg.py <device> <operations> <flags>
	Eg:    python sedcfg.py	/dev/sd? revert psidnumberofthedrive
