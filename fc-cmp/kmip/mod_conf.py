#!/usr/bin/env python
# Module to add/delete lines of text from the p6kmiptool.conf
# file which managers KMIP server(s) for p6r.
# This module will take the input from either the config_kmip configCLI
# configAPI class. configCLI receives inputs via the command line and
# configAPI receives inputs via sys[argv] in the C/C++ fashion to
# support the C/C++ API
#

# Libraries/Modules
import fileinput
import os, sys, subprocess
from shutil import copyfile
#import utils.fccmpLogger
#import utils.yaml_config

sys.path.insert(0, '../utils')

from fccmpLogger import *
from yaml_config import *

#logger = utils.fccmpLogger
#P6Cfg = utils.yaml_config.YAMLConfig.getYAMLConfig()
P6Cfg = YAMLConfig.getYAMLConfig()

class ModP6Conf():

    # add server API
    def addServer(self, address):

        #TODO retrieve the file below location from config.yaml
        file = 'p6kmiptool.conf'
        add_line: bool = True
        line_number: int = 8
        self.address = address
        serverNumber = None

        # backup p6kmiptool.conf  get information from the parameters or pull in from config.yaml
        copyfile(file, file + '.bak')

        if self.address is None:
            sys.exit()
        else:
            if self.address == P6Cfg['server1']:
                serverNumber = 'server1'
            else:
                if self.address == P6Cfg['server2']:
                    serverNumber = 'server2'

        if serverNumber is None:
            sys.exit()
        else:
            self.privateKey = P6Cfg[serverNumber + '_private_key']
            self.caCert = P6Cfg[serverNumber + '_ca_cert']
            self.clientCert = P6Cfg[serverNumber + '_client_cert']
            self.kmipVersion = P6Cfg[serverNumber + '_kmip_version']
            self.sslOptions = P6Cfg[serverNumber + '_kmip_ssl_opts']
            self.InitFlags = P6Cfg[serverNumber + '_kmip_Init_flags']

        # setup the p6kmiptool.com new server entries
        self.insert : str =  '\n[' + self.address + ']\n' \
                        'KMIPPrivPEM="' + self.privateKey + '"\n' \
                        'KMIPRootPEM="' + self.caCert + '"\n' \
                        'KMIPCertPEM="' + self.clientCert + '"\n' \
                        'KMIPMaxVersion="' + str(self.kmipVersion) + '"\n' \
                        'KMIPsslOptions="' + str(self.sslOptions) + '"\n' \
                        'KMIPInitFlags="' + str(self.InitFlags) + '"\n'
        print(self.insert)

        # add new kmip server entry to p6kmiptool.conf file
        # insert 2 blank lines at the end of the new entry
        with open(file, 'r+') as f:
            for line in f:
                if line.startswith("["+self.address+"]"):
                    #print("Found.")
                    add_line = False
                    print(line, end='')
                    f.close()
                    break
        if add_line:
            f = fileinput.input(file, inplace=True)
            for n, line in enumerate(f, start=1):
                if n == line_number:
                    print(self.insert)
                print(line, end='')
            f.close()
        # TODO Make sure the private key, client cert and root cert are actually available
        # TODO ! TEST the subprocess
        # shell out and run p6kmiptool to build keystore KMIP_CMD and KMIP_CMD.sig
        addServer = subprocess.call['p6kmiptool', '-config', '-add-server', self.address]

    # delete kmip server entry
    def delServer(self, address):
        self.address = address
        file = 'p6kmiptool.conf'

        serverNumber = None

        if self.address is None:
            sys.exit()
        else:
            if self.address == P6Cfg['server1']:
                serverNumber = 'server1'
            else:
                if self.address == P6Cfg['server2']:
                    serverNumber = 'server2'

        if serverNumber is None:
            sys.exit()
        else:
            # backup p6kmiptool.conf
            os.rename(file, file + '.bak')

            # setup the lines to be deleted
            self.del1: str = '[' + self.address + ']\n'
            self.del2: str = 'KMIPPrivPEM="' + P6Cfg[serverNumber + '_private_key'] + '"\n'
            self.del3: str = 'KMIPRootPEM="' + P6Cfg[serverNumber + '_ca_cert']+ '"\n'
            self.del4: str = 'KMIPCertPEM="' + P6Cfg[serverNumber + '_client_cert'] + '"\n'
            self.del5: str = 'KMIPMaxVersion="' + str(P6Cfg[serverNumber + '_kmip_version']) + '"\n'
            self.del6: str = 'KMIPsslOptions="' + str(P6Cfg[serverNumber + '_kmip_ssl_opts']) + '"\n'
            self.del7: str = 'KMIPInitFlags="'  + str(P6Cfg[serverNumber + '_kmip_Init_flags']) + '"\n'
            self.del8: str = '\n'

            # open a file, read all the lines
            with open(file + '.bak', "r+") as input:
                with open(file, "w") as output:
                    lines = input.readlines()
                    flag = False
                    subCounter = 0

                    for line in lines:
                        if line.startswith("[" + address + "]"):
                            flag = True
                            print('Flag is ' + str(flag) + ' subCounter is ' + str(subCounter))

                        # NOTE:  7 represents the number of lines a kmip [servername.com] entry has
                        # in the config.yaml -- if that list grows to 8 lines or shrinks to 6
                        # lines this code has to be adjusted !!!! NOT in love with this approach
                        # but it works
                        if flag is True and subCounter <= 7:
                            if flag is True and subCounter == 7 :
                                flag = False
                                print('finished removing lines ')
                            if flag is True:
                                print('removing ' + line)
                            subCounter += 1
                            print('Flag is ' + str(flag) + ' subCounter is ' + str(subCounter))
                            pass

                        if flag is False:
                            print('Flag is ' + str(flag) + ' subCounter is ' + str(subCounter))
                            print('keeping line ' + line)
                            output.write(line)

                output.close()
                input.close()

# This is for testing purposes only
def main():
    #ModP6Conf.addServer(ModP6Conf, 'kmipserver2.com')
    #ModP6Conf.delServer(ModP6Conf, 'kmip-interop1.cryptsoft.com')
    ModP6Conf.addServer(ModP6Conf, 'kmip-interop1.cryptsoft.com')

# This is for testing purposes only
if __name__ == '__main__':
    main()
