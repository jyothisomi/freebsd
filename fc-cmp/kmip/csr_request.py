#!/usr/bin/env python
#
# generate a CSR
#
# Required information
# Certificate Type: Server or Client
# Country Name (2 Letter Code) [US]: US
# State or Province Name (full Name) [Some-State]: Virginia
# Locality Name (e.g., City) [Some-City]: Leesburg
# Organization Name (e.g. Company) [Some-Company]: FUTURA Cyber Inc
# Organizational Unit Name (e.g. Section) [Some-Unit]: Engineering
# Common Name (e.g. server FQDN or Your name) []: John Doe
# Email Address (e.g. user@companyname.com) []: jdoe@futuracyber.com
# OR # subj e.g "/C=US/ST=Georgia/L=Atlanta/O=ABC Company Inc/CN=John Doe"
# set key size
# set encryption type

# Libraries/Modules
import sys
import os
import json
import argparse
from OpenSSL import crypto, SSL
from typing import List, Dict
__path__=[os.path.dirname(os.path.abspath(__file__))]

sys.path.insert(0, '../utils')

#from utils import fccmpLogger
from fccmpLogger import * 

FILEPATH = '/CNCSR'

if not os.path.exists(FILEPATH):
    os.system('mkdir '+FILEPATH)

#logger = fccmpLogger.logger

class X509Certificate():

    #get CLI inputs
    def getCLI(self):
        # retrieve CLI
        parser = argparse.ArgumentParser()
        parser.add_argument("-t", "--type", help="Set certificate type (e.g. Server or Client)", default="client",
                            required=True)
        parser.add_argument("-c", "--country", help="Set 2 letter Country Name (e.g. US)", default='US', required=True)
        parser.add_argument("-s", "--state", help="Set State or Province (e.g. Georgia)", required=True)
        parser.add_argument("-l", "--locality", help="Set Locality (e.g. Atlanta)", required=True)
        parser.add_argument("-o", "--organization", help="Set Organization name (e.g. ABC Company Inc)", required=True)
        parser.add_argument("-u", "--unit", help="Set Organizational Unit (e.g. Accounting)", required=True)
        #parser.add_argument("-n", "--commonname", help="Set Common Name (e.g. hostname.abc-co.com or John Doe)", required=True)
        parser.add_argument("-n", "--name", help="Set Common Name (e.g. hostname.abc-co.com or John Doe)", required=True)
        parser.add_argument("-m", "--email", help="Set Email Address (e.g. jdoe@abc-co.com)", required=True)
        parser.add_argument("-k", "--keysize", help="Set key size (e.g. 1024,2048,4096)", required=True)
        parser.add_argument("-e", "--encryption", help="Set encryption type (e.g. rsa, ec)", required=True)

        # parse into arguments
        arguments = parser.parse_args()

        # convert arguments to dictionary
        args = X509Certificate.args2Dict(self, arguments)
        subjects: Dict = X509Certificate.getCSRSubjects(self, args)
        req = X509Certificate.genCSR(self, subjects, args)
        log_msg('INFO', 'Built X509 Certificate for Common Name: ' + subjects['name'])


    #get API inputs
    def getAPI(self, arguments):
        # process argv which is a json
        # {"type":"client", "country":"US", "state":"Georgia", "locality":"Atlanta", "organization":"ABC Company Inc"
        #  "unit":"Accounting", "name":"John Doe", "email":"jdoe@abc-co.com", "keysize":2048, "encryption":"rsa"}
        #
        arguments: Dict = json.loads(arguments)
        subjects: Dict = X509Certificate.getCSRSubjects(self, arguments)
        req = X509Certificate.genCSR(self, subjects, arguments)
        log_msg('INFO', 'Built X509 Certificate for Common Name: ' + subjects['name'])


    def getCSRSubjects(self, args):
        self.subjects: Dict = {"C": args['country'], "ST":args['state'], "L": args['locality'], "O": args['organization'],
                             "OU": args['unit'], "name": args['name']}
        fields = ['C','ST','L','O','OU','hostname']
        for field in fields:
            try:
                # Check if field is already setup
                if self.subjects[field]:
                    return self.subjects
                log_msg('INFO', 'Fetching CSR Subjects for Common Name: ' + subjects['name'])
            except KeyError:
                print('CSR CLI or API missing item' + self.opts[field] + ' stop processing')
                log_msg('ERROR', 'Error Fetching X509 Certificate for Common Name: ' + subjects['name'])
                # TODO! perhaps this should be break?
                #pass
                break

    # convert arg parser args to dictionary
    def args2Dict(self, args):
        arguments: Dict
        # are arguments return individual CSR inputs or Subject
        #if not args.subject:
        arguments = {'type': args.type,'country': args.country, 'state': args.state, 'locality': args.locality,
            'organization': args.organization, 'unit': args.unit, 'name':  args.name, 'email': args.email}
        #else:
        #    arguments = {'type': args.type, 'subject': args.subject}

        # add keysize and encryption type
        #arguments.update({'keysize', args.keysize})
        arguments['keysize'] = int(args.keysize)
        #arguments.update({'encryption', args.encryption})
        arguments['encryption'] = args.encryption
        log_msg('INFO', 'Converting Args* to Dictionary for X509 Creation for Common Name: ' + args.name)
        return arguments

    def _isCA(self):
        log_msg('INFO', 'Checking if this is a self signed CSR')
        return "TRUE" if self.ca else "FALSE"

    #generate CSR
    def genCSR(self, subjects, args):
        # Set subjects
        self.subjects = subjects

        # Set args
        self.args = args

        # Set hostname
        self.name: str = self.args['name']

        # Set certificate type - Client or Server
        self.type: str = self.args['type']

        # Set keysize 1024, 2048, 4096
        self.keysize: int = self.args['keysize']
        self.encryption: str = self.args['encryption']
        self.ca: bool = False

        # These variables will be used to create the host.csr and host.key files.
        clientName = self.name.replace(" ","").lower()
        csrfile = FILEPATH+'/'+clientName + '.csr'
        keyfile = FILEPATH+'/'+clientName + '.key'

        # OpenSSL Key Type Variable, passed in later.
        if self.encryption == "rsa":
            TYPE_RSA = crypto.TYPE_RSA
        if self.encryption == "ec":
            TYPE_EC = crypto.TYPE_EC

        # setup X509 CSR Request
        req = crypto.X509Req()

        # we already have hostname
        req.get_subject().CN = self.name

        # set mandatory fields
        try:
            req.get_subject().countryName = self.subjects['C']
            req.get_subject().stateOrProvinceName = self.subjects['ST']
            req.get_subject().localityName = self.subjects['L']
            req.get_subject().organizationName = self.subjects['O']
            req.get_subject().organizationalUnitName = self.subjects['OU']
        except KeyError:
            raise Exception('Missing mandatory certificate value!')
            # TODO ! break here?

        # set non-mandatory fields
        try:
            req.get_subject().emailAddress = self.args['email']
        except KeyError:
            pass

        # Add in cert extensions - this is for later if we need it
        '''
        isCA = X509Certificate._isCA(self)
        if isCA:
            EXTCA = b'CA:TRUE, pathlen:0'
        else:
            EXTCA = b'CA:FALSE, pahtlen:0'
        EXTUSAGE = b'keyCertSign, cRLSign'
        base_constraints = ([
            #crypto.X509Extension("keyUsage", False, self.usage),

            crypto.X509Extension(b'basicConstraints', isCA, EXTCA),
            crypto.X509Extension(b'keyUsage', False, EXTUSAGE)
            #crypto.X509Extension("basicConstraints", False, "CA:{c}".format(c=self._isCA(self))),
        ])
        x509_extensions = base_constraints
        
        
        # If there are SAN entries, append the base_constraints to include them.
        # if len(ss):
            #san_constraint = crypto.X509Extension("subjectAltName", False, ss)
            #x509_extensions.append(san_constraint)

        req.add_extensions(x509_extensions)
        '''

        key = X509Certificate.genKey(self, TYPE_RSA, self.keysize)
        req.set_pubkey(key)
        req.sign(key, "sha256")

        # Generate CSRFile
        X509Certificate.generateFiles(self, csrfile, req)

        # Generate KeyFile
        X509Certificate.generateFiles(self, keyfile, key)

        # return the request
        log_msg('INFO', 'Generating the CSR for ' + self.subjects['name'])
        return req

   # generate private Key
    def genKey(self, encryption, keysize):
        self.encryption: str = encryption
        self.keysize: int = keysize
        #Generate Private Key
        key = crypto.PKey()
        key.generate_key(encryption, self.keysize)
        log_msg('INFO', 'Generating a Private Key')
        return key

    # generate files
    def generateFiles(self, mkFile, req):

        #Generate .csr/key files.
        with open(mkFile, "w") as f:
            if ".csr" in mkFile:
                b_csr = crypto.dump_certificate_request(crypto.FILETYPE_PEM, req)
                csr = b_csr.decode("utf-8")
                f.write(csr)
                log_msg('INFO', 'Generating the CSR PEM File')
            elif ".key" in mkFile:
                b_key = crypto.dump_privatekey(crypto.FILETYPE_PEM, req)
                key = b_key.decode('utf-8')
                f.write(key)
                log_msg('INFO', 'Generating the Key PEM File')
            else:
                print('failed to make key')
                log_msg('ERROR', 'FAILED to create the CSR PEM or KEY PEM files')
                # TODO ! write log here


# if run from the command line - calling fc-cmp-cert
#def main(argv):
    #X509Certificate.getCLI(X509Certificate)


#if __name__ == '__main__':
    #main(sys.argv)
