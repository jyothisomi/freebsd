#!/usr/bin/env python
# Module to communicate via KMIP Protocols (1.0, 1.1, 1.2, 1.3, 1.4, 2.0)
# This module will allow verification of the KMIP server, checking of the
# the P6R SDK version, checking of the KMIP protocol versions supported by
# the KMIP server's attached.  Creation, Archive, Retrieval, Query, Revoke
# Destroy, Locate, Activate, Attribute (add, modify, delete)
#

# Libraries/Modules
from typing import List, Dict
import subprocess,sys
#import utils.fccmpLogger
#import utils.yaml_config
import p6pythonkmip
#from utils.kmipconstants import CryptographicUsageMask, CryptographicAlgorithmEnumeration, RevocationReasonCodeEnumeration

sys.path.insert(0, '../utils')
from fccmpLogger import *
from yaml_config import *
from kmipconstants import CryptographicUsageMask, CryptographicAlgorithmEnumeration, RevocationReasonCodeEnumeration

# KMIP Constants from KMIP OASIS specification
CRYPTOUSAGEMASK = CryptographicUsageMask.USAGE_MASK_ENCRYPT | CryptographicUsageMask.USAGE_MASK_DECRYPT

# TODO .. regarding batching -- we will allow multiple commands -- but to START WITH I'm only
# TODO doing single commands

'''
Operational DRAFT:
Class KMIOps exposes all the main KMIP operations necessary to manage and control keys and attributes. This includes:
getP6Version, getKMIPSession, createSymmetricKey, getSymmetricKey, addAttributeToKey, getAttributeKeyCreatedDate, 
getAttributesForKey, getAttributesList, modAttributeForKey, delAttributeForKey, locateKeyInfo, activateKey, revokeKey,
destroyKey, closeSession


Under Development:
getKMIPServerProtocolVersions, archiveKey, recoverKey, QueryKeyInfo

'''
#logger = utils.fccmpLogger
#kmipCfg = utils.yaml_config.YAMLConfig.getYAMLConfig()
kmipCfg = YAMLConfig.getYAMLConfig()

class KMIPOps():

    # get P6 Version Information
    def getP6VersionInfo(self, sessionHandle):
        try:
            self.sessionHandle = sessionHandle
            versionString = p6pythonkmip.getLibraryVersion()
            log_msg('INFO', 'retrievedVersionInfo ' + versionString)
            return versionString
        except Exception as e:
            print('getP6VersionInfo ' + str(e))
            log_msg('ERROR', 'getP6VersionInfo ' + str(e))


    # TODO ! get KMIP Server's Supported Versions (1.0, 1.1, 1.2, 1.3, 1.4, 2.0) - NEED TRY CATCH, Message PASS/FAIL
    # TODO ! How to get KMIP supported versions from hytrust, cryptsoft, SafeNet, Fornetix
    def getKMIPServerProtocolVersions(self):
        try:
            KMIPVersion = kmipCfg['server1_kmip_version']
            print('KMIP Version: ' + KMIPVersion)
            log_msg('INFO', 'KMIP Server Protocol Version ' + KMIPVersion)
        except Exception as e:
            print('getKMIPServerProtocolVersions ' + str(e))
            log_msg('ERROR', 'getKMIPServerProtocolVersions ' + str(e))


    #  get KMIP Server Session ID
    def getKMIPSession(self):
        try:
            # TODO setup YAML configuration to hold servers
            # TODO make sure YAML config and p6kmiptool.conf server
            # TODO matches. Make sure to regen KMIP_CMP, KMIP_CMP
            # TODO keystore using p6kmiptool -config --server-name
            # TODO for any servers stored in p6kmiptool.conf
            KMIPServer = kmipCfg['server1']
            sessionHandle = p6pythonkmip.open(KMIPServer)
            log_msg('INFO', 'Retrieved KMIP Session: True')
            return sessionHandle
        except Exception as e:
            print('getKMIPSession ' + str(e))
            log_msg('ERROR', 'getKMIPSession' + str(e))


    # create AES 256 Symmetric Key with keyUniqueID return String
    def createSymmetricKey(self, sessionHandle, totalKeys, encryptionType, keySize):
        try:
            self.sessionHandle = sessionHandle
            self.totalKeys = totalKeys
            self.encryptionType = encryptionType
            self.keySize = keySize
            keyCount = 1
            keyUniqueIDs = {}

            #check keySize ensure equal to legal size value for AES
            if self.keySize <=64:
                keySize = 64
            if self.keySize > 64 and self.keySize <= 128:
                keySize = 128
            if self.keySize > 128 and self.keySize <= 256:
                keySize = 256
            if self.keySize > 256:
                keySize = 512

            # set the CryptographicAlgorithmEnumeration
            if self.encryptionType == 'AES' or self.encryptionType == 'aes' or self.encryptionType == 'Aes':
                self.eType = CryptographicAlgorithmEnumeration.ALG_AES

            if self.encryptionType == 'EC' or self.encryptionType == 'ec' or self.encryptionType == 'Ec':
                self.eType = CryptographicAlgorithmEnumeration.ALG_AES

            # create all the keys in a dictionary and return when complete
            while not (keyCount > totalKeys):

                keyUniqueIDs[keyCount] = p6pythonkmip.createSymmetricKey(self.sessionHandle, self.eType,
                                                                                       self.keySize, CRYPTOUSAGEMASK)
                keyCount += 1

            log_msg('INFO', 'Created ' + str(totalKeys) + ' Symmetric Keys')
            return keyUniqueIDs
        except Exception as e:
            print('createSymmetricKey ' + str(e))
            log_msg('ERROR', 'createSymmetricKey ' + str(e))


    # get AES 256 Symmetric Key with keyUniqueID return dictionary
    def getSymetricKey(self, sessionHandle, keyUniqueID):
        try:
            self.keyUniqueID = keyUniqueID
            self.sessionHandle = sessionHandle
            keyDict = p6pythonkmip.getSymmetricKey(self.sessionHandle, self.keyUniqueID)
            log_msg('INFO', 'Retrieved Symmetric Key with keyUniqueID: ' + self.keyUniqueID)
            return keyDict
        except Exception as e:
            print('getSymetricKey ' + str(e))
            log_msg('ERROR', 'getSymetricKey ' + str(e))


    # add attributes to key return dictionary
    def addAttributeToKey(self, sessionHandle, keyUniqueID, SEDSN, SEDAuthority, PINVersion):
        try:
            self.sessionHandle = sessionHandle
            self.keyUniqueID = keyUniqueID
            self.SEDSN = SEDSN
            self.SEDAuthority = SEDAuthority
            self.PINVersion = PINVersion

            # attribute labels
            xSEDSN = "x-SEDSN"
            xSEDAuthority = "x-SEDAuthority"
            xPINVersion = "x-PINVersion"

            #test data
            p6pythonkmip.addTextAttribute(self.sessionHandle, self.keyUniqueID, xSEDSN, self.SEDSN)
            p6pythonkmip.addTextAttribute(self.sessionHandle, self.keyUniqueID, xSEDAuthority, self.SEDAuthority)
            p6pythonkmip.addTextAttribute(self.sessionHandle, self.keyUniqueID, xPINVersion, str(self.PINVersion))

            # now retrieve attributes for the key
            xAttr = {}
            xAttr[xSEDSN] = p6pythonkmip.getAttribute(self.sessionHandle, self.keyUniqueID, xSEDSN)
            xAttr[xSEDAuthority] = p6pythonkmip.getAttribute(self.sessionHandle, self.keyUniqueID, xSEDAuthority)
            xAttr[xPINVersion] = p6pythonkmip.getAttribute(self.sessionHandle, self.keyUniqueID, xPINVersion)
            log_msg('INFO', 'Retrieved Attributes for key: ' + self.keyUniqueID + ' x-SEDSN=' + xAttr[xSEDSN][0]
                           + ' x-SEDAuthority=' + xAttr[xSEDAuthority][0] + ' x-PINVersion' + str(xAttr[xPINVersion][0]))
            return xAttr
        except Exception as e:
            print('addAttributeToKey ' + str(e))
            log_msg('ERROR', 'addAttributeToKey ' + str(e))


    # get attributed key created date
    def getAttributeKeyCreatedDate(self, sessionHandle, keyUniqueID):
        try:
            self.sessionHandle = sessionHandle
            self.keyUniqueID = keyUniqueID
            # show how to get the value of a single instance attribute
            createdDate = p6pythonkmip.getAttribute(sessionHandle, self.keyUniqueID, "Original Creation Date")
            log_msg('INFO', 'Retrieved Attribute Key Created Date for key: ' + self.keyUniqueID +
                           ' Created Date:' + createdDate[0])
            return createdDate
        except Exception as e:
            print('getAttributeKeyCreatedDate ' + str(e))
            log_msg('ERROR', 'getAttributeKeyCreatedDate '+ str(e))


    # get attributes for a key and return dictionary
    def getAttributesForKey(self, sessionHandle, keyUniqueID):
        try:
            self.sessionHandle = sessionHandle
            self.keyUniqueID = keyUniqueID

            # attribute labels
            xSEDSN = "x-SEDSN"
            xSEDAuthority = "x-SEDAuthority"
            xPINVersion = "x-PINVersion"

            # now retrieve attributes for the key
            xAttr = {}
            xAttr[xSEDSN] = p6pythonkmip.getAttribute(self.sessionHandle, self.keyUniqueID, xSEDSN)
            xAttr[xSEDAuthority] = p6pythonkmip.getAttribute(self.sessionHandle, self.keyUniqueID, xSEDAuthority)
            xAttr[xPINVersion] = p6pythonkmip.getAttribute(self.sessionHandle, self.keyUniqueID, xPINVersion)

            log_msg('INFO', 'Retrieved Attributes for key: ' + self.keyUniqueID + ' x-SEDSN=' + xAttr[xSEDSN][0]
                           + ' x-SEDAuthority=' + xAttr[xSEDAuthority][0] + ' x-PINVersion' + str(xAttr[xPINVersion][0]))
            return xAttr
        except Exception as e:
            print('getAttributesForKey ' + str(e))
            log_msg('ERROR', 'getAttributesForKey ' + str(e))


    # get attributes list and return attribute list
    def getAttributesList(self, sessionHandle, keyUniqueID):
        try:
            self.sessionHandle = sessionHandle
            self.keyUniqueID = keyUniqueID

            # show how all attributes of an object are returned
            attributeList = p6pythonkmip.getAllAttributes(self.sessionHandle, self.keyUniqueID)
            log_msg('INFO', 'Retrieved All Attributes for key: ' + self.keyUniqueID + ' x-SEDSN=' +
                           attributeList[0] + ' x-SEDAuthority=' + attributeList[1] + ' x-PINVersion=' +
                           attributeList[2])
            return attributeList
        except Exception as e:
            print('getAttributesList ' + str(e))
            log_msg('ERROR', 'getAttributesList ' + str(e))


    # modify attributes and return changed attribute
    def modAttributeForKey(self, sessionHandle, keyUniqueID, attribName, attribValue):
        try:
            self.sessionHandle = sessionHandle
            self.keyUniqueID = keyUniqueID
            self.attribName = attribName
            self.attribValue = attribValue

            # show how to modify existing attribute values
            p6pythonkmip.modifyTextAttribute(self.sessionHandle, self.keyUniqueID, self.attribName, self.attribValue, 0)

            # retrieve the changed attribute
            changedAttrib = p6pythonkmip.getAttribute(self.sessionHandle, self.keyUniqueID, self.attribName)
            log_msg('INFO', 'Changed Attribute for key: ' + self.keyUniqueID + ' ' +
                           attribName + '= ' + changedAttrib[0])
            return changedAttrib
        except Exception as e:
            print('modAttributeForKey ' + str(e))
            log_msg('ERROR', 'modAttributeForKey ' + str(e))


    # delete attributes and return deleted attribute
    def delAttributeForKey(self, sessionHandle, keyUniqueID, attribName):
        try:
            self.sessionHandle = sessionHandle
            self.keyUniqueID = keyUniqueID
            self.attribName = attribName

            # show how to delete an instance of an attribute
            p6pythonkmip.deleteAttribute(self.sessionHandle, self.keyUniqueID, self.attribName, 0)
            deletedAttrib = p6pythonkmip.getAttribute(self.sessionHandle, self.keyUniqueID, self.attribName)
            log_msg('INFO', 'Deleted Attribute for key: ' + self.keyUniqueID +' ' + self.attribName)
            if deletedAttrib is None:
                return True
            else:
                return False
        except Exception as e:
            #TODO ! should this be finally?
            log_msg('ERROR', 'delAttributeForKey ' + str(e))
            print('delAttributeForKey ' + str(e))
            return False


    # locate Key by test attribute returned as list
    def locateKeyInfo(self, sessionHandle, attribName, attribValue):
        try:
            self.sessionHandle = sessionHandle
            self.attribName= attribName
            self.attribValue = attribValue

            keysByAttrib = p6pythonkmip.locateByTextAttribute(self.sessionHandle, self.attribName, self.attribValue)
            for key in keysByAttrib:
                log_msg('INFO', 'Located key: ' + key + ' using ' + self.attribName)
            return keysByAttrib
        except Exception as e:
            log_msg('ERROR', 'locateKeyInfo ' + str(e))
            print('locateKeyInfo' + str(e))


    # activate a Key
    def activateKey(self, sessionHandle, keyUniqueID):
        try:
            self.sessionHandle = sessionHandle
            self.keyUniqueID = keyUniqueID

            # a key must be activated to use it for derivation
            p6pythonkmip.activate(self.sessionHandle, self.keyUniqueID);
            log_msg('INFO', 'Activated key: ' + self.keyUniqueID)
        except Exception as e:
            log_msg('ERROR', 'activateKey ' + str(e))
            print('activateKey ' + str(e))


    # revoke Key
    def revokeKey(self, sessionHandle, keyUniqueID, revokeCode, revokeReason):
        try:
            self.sessionHandle = sessionHandle
            self.keyUniqueID = keyUniqueID
            self.revokeCode = revokeCode
            self.revokeReason = revokeReason

            # activate the key to make sure it can be revoked
            p6pythonkmip.activate(self.sessionHandle, self.keyUniqueID);
            # revoke the key
            p6pythonkmip.revoke(self.sessionHandle, self.keyUniqueID,
                                self.revokeCode, self.revokeReason)
            log_msg('INFO', 'Revoked key: ' + self.keyUniqueID)

            return True

        # TODO! should I use finally here instead of except?
        except Exception as e:
            log_msg('ERROR', 'revokeKey ' + str(e))
            print('revokeKey ' + str(e))
            return False


    # destroy Key
    def destroyKey(self, sessionHandle, keyUniqueID):
        try:
            self.sessionHandle = sessionHandle
            self.keyUniqueID = keyUniqueID
            p6pythonkmip.destroy(self.sessionHandle, self.keyUniqueID)
            log_msg('INFO', 'Destroyed key: ' + self.keyUniqueID)
            return True
        except Exception as e:
            log_msg('ERROR', 'destroyKey ' + str(e))
            print('destroyKey ' + str(e))
            return False


    # TODO !Archive Key- P6 doesn't appear to support this (we must build it)
    def archiveKey(self, sessionHandle, keyUniqueID):
        self.keyUniqueID = keyUniqueID


    # TODO !Recover Key- P6 doesn't appear to support this (we must build it)
    def recoverKey(self, sessionHandle, keyUniqueID):
        self.keyUniqueID = keyUniqueID


    # TODO !Query Key Info- NEED TRY CATCH, Message PASS/FAIL (we must build this from KMIP C++ ex 21
    def queryServerInfo(self, address):
        self.address = address
        # shell out and run p6kmiptool to generate query info re server 1 or server 2
        # using p6kmiptool -serverInfo {1|2} -server <FQDN>
        queryInfo = subprocess.call['p6kmiptool', '-serverInfo', 1, '-add-server', self.address]
        # TODO Make this return a JSON formatted string
        print(queryInfo)

    # close session return bool as status
    def closeSession(self, sessionHandle):
        try:
            self.sessionHandle = sessionHandle
            p6pythonkmip.close(self.sessionHandle)
            log_msg('INFO', 'Closed KMIP Session')
            return True
        except Exception as e:
            log_msg('ERROR', 'closeSession ' + str(e))
            print('closeSession ' + str(e))
            return False

    # verify KMIP Server in lieu of Query - pretty much tests every function this server supports
    def verifyServer(self):

        # Set local variables
        queryInfo: Dict = {}
        SEDSN0 = '0000000012345678'
        SEDAuthority0 = 'bandmaster123456'
        PINVersion0 = 1
        SEDSN1 = '0000000087654321'
        SEDAuthority1 = 'bandmaster654321'
        PINVersion1 = 9

        # Verify Connection
        try:
            self.sessionHandle = KMIPOps.getKMIPSession(self)
            log_msg('INFO', 'Verify Server Session')
            queryInfo['Server Session'] = True
        except Exception as e:
            queryInfo['Server Session'] = False
            log_msg('ERROR', 'Verify Connection getKMIPSession ' + str(e))
            print('Verify Connection getKMIPSession ' + str(e))
            pass

        # Verify P6 Version
        try:
            testP6version = KMIPOps.getP6VersionInfo(self, self.sessionHandle)
            log_msg('INFO', 'Verify P6 Version' + testP6version)
            queryInfo['P6R Version'] = True
        except Exception as e:
            queryInfo['P6R Version'] = False
            log_msg('ERROR', 'Verify Connection getP6VersionInfo ' + str(e))
            print('Verify Connection getP6VersionInfo ' + str(e))
            pass

        # Very Create Key
        try:
            self.testSKUniqueID = KMIPOps.createSymmetricKey(self, self.sessionHandle, 1, 'AES', 256)
            log_msg('INFO', 'Verify Server Create Symmetric Key: ' + self.testSKUniqueID[1] )
            queryInfo['Create Symmetric Key'] = True
        except Exception as e:
            queryInfo['Create Symmetric Key'] = False
            log_msg('ERROR', 'Verify Connection createSymmetricKey ' + str(e))
            print('Verify Connection createSymmetricKey ' + str(e))
            pass

        # Test Retrieve Key
        try:
            testRetrieve = KMIPOps.getSymetricKey(self, self.sessionHandle, self.testSKUniqueID[1])
            log_msg('INFO', 'Verify Server Retrieved Symmetric Key Information for key :' + self.testSKUniqueID[1])
            queryInfo['Retrieve Symmetric Key'] = True
        except Exception as e:
            queryInfo['Retrieve Symmetric Key'] = False
            log_msg('ERROR', 'Verify Connection getSymmetricKey ' + str(e))
            print('Verify Connection getSymmetricKey ' + str(e))
            pass

        # Test Add Attribute
        try:
            testAddAttrib = KMIPOps.addAttributeToKey(self, self.sessionHandle, self.testSKUniqueID[1], SEDSN0,
                                                          SEDAuthority0, PINVersion0)
            log_msg('INFO', 'Verify Server Add Attribute for Symmetric Key: ' + self.testSKUniqueID[1] + ' x-SEDSN='
                       + SEDSN0 +' x-SEDAuthority=' + SEDAuthority0 + ' x-PINAuthority=' + str(PINVersion0))
            queryInfo['Add Attribute to Key'] = True
        except Exception as e:
            queryInfo['Add Attribute to Key'] = False
            log_msg('ERROR', 'Verify Connection addAttributeToKey ' + str(e))
            print('Verify Connection addAttributeToKey ' + str(e))
            pass

        # Test Attribute Get Key CreatedDate
        try:
            testCreatedDate = KMIPOps.getAttributeKeyCreatedDate(self, self.sessionHandle, self.testSKUniqueID[1])
            log_msg('INFO', 'Verify Server Get Key Create Date for key: ' + self.testSKUniqueID[1] + ' Created Date='
                       + testCreatedDate[0])
            queryInfo['Attribute Creation Date'] = True
        except Exception as e:
            queryInfo['Attribute Creation Date'] = False
            log_msg('ERROR', 'Verify Connection getAttributeKeyCreatedDate ' + str(e))
            print('Verify Connection getAttributeKeyCreatedDate ' + str(e))
            pass

        # Test Get Specific Key Attributes for FC-CMP
        try:
            testGetAttributes = KMIPOps.getAttributesForKey(self, self.sessionHandle, self.testSKUniqueID[1])
            log_msg('INFO', 'Verify Server Get Attributes for Key: ' + self.testSKUniqueID[1])
            log_msg('INFO', testGetAttributes)
            queryInfo['FC-CMP Specific Attributes '] = True
        except Exception as e:
            queryInfo['FC-CMP Specific Attributes '] = False
            log_msg('ERROR', 'Verify Connection getAttributesForKey ' + str(e))
            print('Verify Connection getAttributesForKey ' + str(e))
            pass

        # Test Get key Attributes List
        try:
            testGetAttributesList = KMIPOps.getAttributesList(self, self.sessionHandle, self.testSKUniqueID[1])
            log_msg('INFO', 'Verify Server Get Attribute List for key: ' + self.testSKUniqueID[1])
            log_msg('INFO', testGetAttributesList)
            queryInfo['Attribute List'] = True
        except Exception as e:
            queryInfo['Attribute List'] = False
            log_msg('ERROR', 'Verify Connection getAttributesList ' + str(e))
            print('Verify Connection getAttributesList ' + str(e))
            pass

        # Test key Locate by Text Attributes
        try:
            testLocate =  KMIPOps.locateKeyInfo(self, self.sessionHandle, 'x-SEDSN', SEDSN0)
            log_msg('INFO', 'Verify Server Test Locate by Text Attribute for Symmetric Key: ' + self.testSKUniqueID[1])
            queryInfo['Locate Keys By Text Attribute'] = True
        except Exception as e:
            queryInfo['Locate Keys By Text Attribute'] = False
            log_msg('ERROR', 'Verify Connection locateKeyInfo '+ str(e))
            print('Verify Connection locateKeyInfo ' + str(e))
            pass

        # Test Key Modify Attribute
        try:
            testModAttrib = KMIPOps.modAttributeForKey(self, self.sessionHandle, self.testSKUniqueID[1],
                                                                        'x-SEDSN', SEDSN1)
            log_msg('INFO', 'Verify Server Modify Attribute for Symmetric Key: ' + self.testSKUniqueID[1])
            queryInfo['Modify Key Attribute'] = True
        except Exception as e:
            queryInfo['Modify Key Attribute'] = False
            log_msg('ERROR', 'Verify Connection modAttributeForKey ' + str(e))
            print('Verify Connection modAttributeForKey ' + str(e))
            pass

        # Test Key Delete Attribute
        try:
            testDelAttrib = KMIPOps.delAttributeForKey(self, self.sessionHandle, self.testSKUniqueID[1],
                                                                         'x-SEDAuthority')
            log_msg('INFO', 'Verify Server Delete Attribute Symmetric Key: ' + self.testSKUniqueID[1])
            queryInfo['Delete Key Attribute'] = True
        except Exception as e:
            queryInfo['Delete Key Attribute'] = True
            log_msg('ERROR', 'Verify Connection delAttributeForKey ' + str(e))
            print('Verify Connection delAttributeForKey ' + str(e))
            pass

        # Test Revoke, Activate, Destroy Key

        try:
            testRevokeKey = KMIPOps.revokeKey(self, self.sessionHandle, self.testSKUniqueID[1],
                            RevocationReasonCodeEnumeration.REVOCATION_CESSATION_OF_OPERATION, 'Verification')
            log_msg('INFO', 'Verify Server Activate Symmetric Key: ' + self.testSKUniqueID[1])
            log_msg('INFO', 'Verify Server Revoke Symmetric Key: ' + self.testSKUniqueID[1])
            queryInfo['Activate Key'] = True
            queryInfo['Revoke Key'] = True
        except Exception as e:
            queryInfo['Activate Key'] = False
            queryInfo['Revoke Key'] = False
            log_msg('ERROR', 'Verify Connection revokeKey ' + str(e))
            print('Verify Connection revokeKey ' + str(e))
            pass

        # Test Destroy Key
        try:
            testDestroyKey = KMIPOps.destroyKey(self, self.sessionHandle, self.testSKUniqueID[1])
            log_msg('INFO', 'Verify Server Destroy Symmetric Key: ' + self.testSKUniqueID[1])
            queryInfo['Destroy Key'] = True
        except Exception as e:
            queryInfo['Destroy key'] = False
            log_msg('ERROR', 'Verify Connection destroyKey ' + str(e))
            print('Verify Connection destroyKey ' + str(e))
            pass


        # close the session
        try:
            closeTestSessionHandle = KMIPOps.closeSession(self, self.sessionHandle)
            log_msg('INFO', 'Verify Server Closed KMIP Server session:' + self.testSKUniqueID[1])
        except Exception as e:
            log_msg('ERROR', 'Verify Connection closeTestSession ' + str(e))
            print('Verify Connection closeTestSession ' + str(e))

        # Print Verify Server Info
        log_msg('INFO', 'Printing Verify Server Information')
        print('Verifying KMIP Server Capabilities:')
        log_msg('INFO', '__________________________________________')
        print('__________________________________________')
        for query in queryInfo:
            print('{0:<32s} {1:>10s}'.format('| ' + query, str(queryInfo[query]) +' |'))
            log_msg('INFO', 'Verify Server KMIP Function: ' + '{0:<32s} {1:>10s}'.format('| ' + query,
                                                                                        str(queryInfo[query]) +' |'))
        print('__________________________________________')
        log_msg('INFO', '__________________________________________')
        return queryInfo

# This is for testing purposes only
def main():
    queryInfo = KMIPOps.verifyServer(KMIPOps)


# This is for testing purposes only
if __name__ == '__main__':
    main()
